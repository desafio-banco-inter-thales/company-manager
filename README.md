<center>
  <p align="center">
    <img src="https://gitlab.com/desafio-banco-inter-thales/user-manager/-/raw/main/public/logo.png" width="150"/>&nbsp;
  </p>  
  <h1 align="center">🚀 Microserviço: Gerenciador de empresas</h1>
  <p align="center">
    Microserviço referente ao desafio banco inter para criação de uma mini<br /> 
    plataforma de investimentos utilizando clean arch, DDD, 12 Factor App e as<br />
    boas práticas atuais do mercado. Espero que gostem!!
  </p>
</center>
<br />

## Objetivo do microserviço

A idéia desse microserviço é gerenciar as empresas que serão lidas pelo próximo
microserviço de gerenciamento de ações.

## Ferramentas necessárias para rodar o projeto em container

- Docker
- Docker compose

## Ferramentas necessárias para o projeto localmente

- IDE de sua preferência
- Navegador de sua preferência
- JDK 17
    - Utilizei o sdkman para instalação da versão **17.0.3-zulu**
- Docker e Docker compose
    - Para rodar as dependências
- Gradle
    - Utilizei o sdkman para instalação da versão **7.5**

## Pré-execução

1. Clonar o repositório:
```sh
git clone https://gitlab.com/desafio-banco-inter-thales/company-manager.git
```

2. Entre na pasta referente ao repositório:
```sh
cd company-manager
```

## Execução

> Disclaimer 1 : É necessário ter ao menos um usuário cadastrado na base do
> tipo administrador para gerencias as empresas


> Disclaimer 2: No arquivo **src/main/resources/data.sql** existe
> um insert comentado que podem te auxiliar a não precisar passar pelo
> fluxo de criação de usuário. Para utilizá-lo basta descomentar a linha 21

É possível executar o projeto de duas formas:

### Apenas containers

1. Execute o comando
```shell
docker-compose -f containers/docker-compose.yml up -d --build
```

2. Abra o navegador em http://localhost:8080/swagger-ui/index.html

> É importante que você tenha criado ao menos um usuário administrador
> Para mais detalhes leia a [documentação do user-manager](https://gitlab.com/desafio-banco-inter-thales/user-manager)

4. Abra o navegador em http://localhost:8081/swagger-ui/index.html

> Esse é o swagger referente ao company-manager

### Aplicação rodando localmente

1. Execute o comando
```shell
docker-compose -f containers/docker-compose-deps.yml up -d --build
```
> A opção **--build** serve para não utilizarmos o cache para construção
> da imagem do user-manager, caso queira buildar uma imagem já tendo gerado
> a primeira e não quer perder tempo pode remover essa opção de --build

2. Execute a aplicação
```shell
./gradlew bootRun
```
> Também é possível executar como uma aplicação Java através do
> método main() na classe Main.java

3. Abra o navegador em http://localhost:8081/swagger-ui/index.html

## Banco de dados
O banco de dados principal é um H2 e para subir localmente não precisamos de
passos extras, a própria aplicação se encarrega de subir as tabelas descritas
no arquivo presente em ```src/main/resources/data.sql```

> É importante mencionar que pela forma que o sistema foi escrito
> toda vez que a aplicação é derrubada nossos registros serão removidos
> do banco

## Stop

Caso queira derrubar a aplicação

1. Execute o comando
```shell
docker-compose -f containers/docker-compose.yml down
```

## Testes

1. Execute o comando
```shell
./gradlew test
```

> Também é possível executar via IDE. Utilizando o Intellij vá até a pasta
> **src/test/java/com/desafio** clique com o botão direito e na metade
> das opções apresentadas escolha **Run Tests in 'com.desafio''**

## Responsabilidades do microserviço

- [x] Criação de empresas
- [x] Atualização do preço das empresas
- [x] Deleção de empresas
- [x] Ativação de empresas
- [x] Desativação de empresas
- [x] Listagem paginada de empresas
- [x] Listagem paginada de empresas por status
- [x] Busca de uma empresa específica
- [x] Consumo de um payload do usuário pelo o tópico ```USER_CREATED```
- [x] Envio de um payload da empresa para o tópico ```COMPANY_EVENT```
  - Este podendo ser do tipo **save** ou **delete**
 
## Design do microserviço

Exemplo criação de empresa
<br/>
<center>
  <p align="center">
    <img src="https://gitlab.com/desafio-banco-inter-thales/company-manager/-/raw/main/public/desafio-CompanyMan-Final.drawio.png"/>&nbsp;
  </p>  
</center>

### Características do microserviço

- [x] Utilizado comunicação síncrona na entrada de requisições
    - Para facilitar a criação de novos usuários
- [x] Utilizado comunicação assíncrona na saída de eventos
    - Spring cloud stream com binder do kafka
- [x] Utilizado H2 como banco de dados
- [x] Utilizado **Sleuth** para o tracing entre as aplicações
- [x] Utilizado **Zipkin** para visualização do tracing entre as aplicações
- [x] Docker para containerização
- [x] Code coverage com [sonarcloud ~ 75%](https://sonarcloud.io/project/overview?id=desafio-banco-inter-thales_company-manager)

#### Pontos negativos e falhas do microserviço


1. A aplicação não consome os eventos desde o início então se houver a criação 
de 5 usuários antes do company-manager estar de pé, esses usuários não estarão
cadastrados no company-manager;
2. Usuários administradores podem apagar qualquer empresa e não somente a que
eles criaram.

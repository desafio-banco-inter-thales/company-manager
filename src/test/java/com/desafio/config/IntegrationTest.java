package com.desafio.config;

import com.desafio.infrastructure.configuration.ObjectMapperConfig;
import com.desafio.infrastructure.configuration.WebServerConfig;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@ActiveProfiles("test")
//@Import(ObjectMapperConfig.class)
@SpringBootTest(classes = WebServerConfig.class)
public @interface IntegrationTest { }
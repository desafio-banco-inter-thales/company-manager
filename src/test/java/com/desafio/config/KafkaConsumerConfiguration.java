package com.desafio.config;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;

import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.singleton;

@IntegrationTest
public abstract class KafkaConsumerConfiguration {

  static KafkaContainer kafka =
          new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:6.2.1"));

  public static Consumer<String, String> kafkaConsumer;

  @BeforeAll
  public static void setUp() {
    Map<String, Object> configs =
            new HashMap<>(KafkaTestUtils.consumerProps(kafka.getBootstrapServers(), "test", "true"));
    configs.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
    kafkaConsumer =
            new DefaultKafkaConsumerFactory<>(
                    configs, new StringDeserializer(), new StringDeserializer())
                    .createConsumer();
    kafkaConsumer.subscribe(singleton("COMPANY_EVENT"));
  }

  @AfterAll
  public static void shutdown() {
    kafkaConsumer.close();
  }

  static {
    kafka.start();
  }

  @DynamicPropertySource
  public static void overrideProps(DynamicPropertyRegistry registry) {
    registry.add("spring.cloud.stream.kafka.binder.brokers", () -> kafka.getHost());
    registry.add(
            "spring.cloud.stream.kafka.binder.defaultBrokerPort", () -> kafka.getFirstMappedPort());
  }

}

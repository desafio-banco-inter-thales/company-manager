package com.desafio.infrastructure.api;

import com.desafio.application.company.CompanyDto;
import com.desafio.application.company.create.CreateCompanyOutput;
import com.desafio.application.user.create.CreateUserOutput;
import com.desafio.config.ControllerTest;
import com.desafio.domain.company.Company;
import com.desafio.domain.user.User;
import com.desafio.infrastructure.api.request.CreateCompanyRequest;
import com.desafio.infrastructure.api.response.CompanyOutput;
import com.desafio.infrastructure.api.service.*;
import com.desafio.infrastructure.stream.EventProducer;
import com.desafio.shared.notification.Notification;
import com.desafio.shared.notification.NotificationErrorProps;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Objects;

import static com.desafio.shared.utils.Constants.COMPANY_STR;
import static com.desafio.shared.utils.Constants.NAME_STR;
import static com.desafio.shared.utils.ErrorConstants.STRING_SHOULD_NOT_BE_NULL;
import static com.desafio.shared.utils.Samples.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ControllerTest(controllers = CompanyApi.class)
public class CompanyApiTest {

  @Autowired
  private MockMvc mvc;

  @Autowired
  private ObjectMapper mapper;

  @MockBean
  private CreateCompanyService createCompanyService;

  @MockBean
  private UpdateCompanyService updateCompanyService;

  @MockBean
  private ActivateCompanyService activateCompanyService;

  @MockBean
  private DeactivateCompanyService deactivateCompanyService;

  @MockBean
  private DeleteCompanyService deleteCompanyService;

  @MockBean
  private ListCompaniesService listCompaniesService;

  @MockBean
  private FindCompanyByTickerService findCompanyByTickerService;

  @MockBean
  private EventProducer eventProducer;

  public final String ROUTE_COMPANIES = "/companies";

  @Test
  public void givenAValidCommand_whenCallsCreateCompany_shouldReturnUserId() throws Exception {

    final var requestBody = new CreateCompanyRequest(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, CPF_SAMPLE, true);

    final var company = Company.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE);
    final var companyDto = CompanyDto.of(company);
    when(createCompanyService.execute(requestBody))
            .thenReturn(CompanyOutput.of(companyDto));

    final var request = post(ROUTE_COMPANIES)
            .contentType(MediaType.APPLICATION_JSON)
            .content(this.mapper.writeValueAsString(requestBody));

    final var userLocation = ROUTE_COMPANIES.concat("/").concat(companyDto.ticker());
    final var response = this.mvc.perform(request)
            .andDo(print())
            .andExpectAll(
                    status().isCreated(),
                    header().string("Location", userLocation)
            );


    response.andExpect(status().isCreated())
            .andExpect(header().string("Location", userLocation));

    verify(createCompanyService, times(1)).execute(any());
  }

  @Test
  public void givenAInvalidCommandNullName_whenCallsCreateCompany_shouldReturnError() throws Exception {

    final var requestBody = new CreateCompanyRequest(null, TICKER_SAMPLE, PRICE_SAMPLE, CPF_SAMPLE, true);

    final var mockedNotificationErrors = new Notification();
    mockedNotificationErrors.append(new NotificationErrorProps(STRING_SHOULD_NOT_BE_NULL.replace("{}", NAME_STR), COMPANY_STR));

    when(createCompanyService.execute(any()))
            .thenReturn(CompanyOutput.of(mockedNotificationErrors));

    final var request = post(ROUTE_COMPANIES)
            .contentType(MediaType.APPLICATION_JSON)
            .content(this.mapper.writeValueAsString(requestBody));

    this.mvc.perform(request)
            .andDo(print())
            .andExpect(status().isUnprocessableEntity())
            .andExpect(header().string("Location", nullValue()))
            .andExpect(header().string("Content-Type", MediaType.APPLICATION_JSON_VALUE));

    verify(createCompanyService, times(1)).execute(argThat(cmd ->
            Objects.equals(null, cmd.name()) && Objects.equals(CPF_SAMPLE, cmd.userCpf())
    ));
  }

}

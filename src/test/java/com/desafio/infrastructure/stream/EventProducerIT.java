package com.desafio.infrastructure.stream;

import com.desafio.application.company.create.CreateCompanyUseCase;
import com.desafio.application.company.delete.DeleteCompanyCommand;
import com.desafio.application.company.delete.DeleteCompanyUseCase;
import com.desafio.application.user.create.CreateUserCommand;
import com.desafio.application.user.create.CreateUserOutput;
import com.desafio.application.user.create.CreateUserUseCase;
import com.desafio.config.KafkaConsumerConfiguration;
import com.desafio.infrastructure.api.request.CreateCompanyRequest;
import com.desafio.infrastructure.api.service.CreateCompanyService;
import com.desafio.infrastructure.api.service.DeleteCompanyService;
import com.desafio.infrastructure.api.service.UpdateCompanyService;
import com.desafio.infrastructure.gateway.user.UserJpaEntity;
import com.desafio.infrastructure.gateway.user.UserRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import static com.desafio.shared.utils.Constants.*;
import static com.desafio.shared.utils.Samples.*;

public class EventProducerIT extends KafkaConsumerConfiguration {

  @Autowired
  CreateCompanyService createCompanyService;

  @Autowired
  DeleteCompanyUseCase deleteCompanyUseCase;

  @Autowired
  DeleteCompanyService deleteCompanyService;

  @Autowired
  UpdateCompanyService updateCompanyService;

  @Autowired
  CreateCompanyUseCase createCompanyUseCase;

  @Autowired
  CreateUserUseCase createUserUseCase;

  private static final ObjectMapper mapper = new ObjectMapper();

  CreateUserOutput output;

  @Autowired
  UserRepository repository;


  @BeforeEach
  void setup() {
    output = createUserUseCase.execute(CreateUserCommand.of(USER_NAME_SAMPLE, CPF_SAMPLE, true));
    deleteCompanyUseCase.execute(DeleteCompanyCommand.of(TICKER_SAMPLE));
  }

  @AfterEach
  void shutDown() {
    repository.delete(UserJpaEntity.of(output.id()));
  }

  /***
   * Para resolver problema de compressão do gitlab 'Could not initialize class org.xerial.snappy.Snappy' devo adicionar
   * o comando 'RUN apk add gcompat' segundo https://wiki.alpinelinux.org/wiki/Running_glibc_programs. Descobrir depois
   * onde colocar
   */

//  @Test
//  void givenAValidCommand_whenCallsCreateCompany_itProduceAnEvent() throws IOException {
//    final var aCommand = new CreateCompanyRequest(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, CPF_SAMPLE, true);
//    createCompanyService.execute(aCommand);
//    ConsumerRecord<String, String> singleRecord =
//            KafkaTestUtils.getSingleRecord(
//                    kafkaConsumer, COMPANY_EVENT, KAFKA_TIMEOUT);
//    Assertions.assertNotNull(singleRecord);
//
//    Map<String, String> output = mapper.readValue(singleRecord.value(), Map.class);
//    Assertions.assertEquals(TICKER_SAMPLE, output.get("ticker"));
//  }
//
//  @Test
//  void givenAValidCommand_whenCallsDeleteCompany_itProduceAnEvent() throws IOException {
//    final var aCommand = CreateCompanyCommand.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true);
//    createCompanyUseCase.execute(aCommand);
//    deleteCompanyService.execute(TICKER_SAMPLE, CPF_SAMPLE);
//
//    ConsumerRecord<String, String> singleRecord =
//            KafkaTestUtils.getSingleRecord(
//                    kafkaConsumer, COMPANY_EVENT, KAFKA_TIMEOUT);
//    Assertions.assertNotNull(singleRecord);
//
//    Map<String, String> output = mapper.readValue(singleRecord.value(), Map.class);
//    Assertions.assertEquals("delete", output.get("type"));
//  }
//
//  @Test
//  void givenAValidCommand_whenCallsUpdateCompany_itProduceAnEvent() throws IOException {
//    final var expectedEvent = "{\"type\":\"save\",\"id\":null,\"name\":\"Inter\",\"ticker\":\"BIDI11\",\"price\":1.34,\"isActive\":true,\"basicEventData\":\"[type=save, name=Inter, ticker=BIDI11, price=1.34, isActive=true]\"}";
//    final var aCommand = CreateCompanyCommand.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true);
//    createCompanyUseCase.execute(aCommand);
//    updateCompanyService.execute(TICKER_SAMPLE, new UpdateCompanyRequest(BigDecimal.valueOf(1.34), CPF_SAMPLE));
//
//    ConsumerRecord<String, String> singleRecord =
//            KafkaTestUtils.getSingleRecord(
//                    kafkaConsumer, COMPANY_EVENT, KAFKA_TIMEOUT);
//    Assertions.assertNotNull(singleRecord);
//
//    Assertions.assertEquals(expectedEvent, singleRecord.value());
//  }

  @Test
  void givenInvalidCommandWithNoUser_whenCallsCreateCompany_itDoesNothing() throws Exception {

    final var aCommand = new CreateCompanyRequest(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, "123", true);
    createCompanyService.execute(aCommand);

    final var actualException =
            Assertions.assertThrows(
                    IllegalStateException.class,
                    () -> KafkaTestUtils.getSingleRecord(kafkaConsumer, COMPANY_EVENT, KAFKA_TIMEOUT));

    Assertions.assertEquals(NO_RECORDS_FOUND_FOR_TOPIC, actualException.getMessage().toUpperCase() + ".");
  }

}

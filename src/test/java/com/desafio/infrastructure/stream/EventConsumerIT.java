package com.desafio.infrastructure.stream;

import com.desafio.config.IntegrationTest;
import com.desafio.config.KafkaProducerConfiguration;
import com.desafio.infrastructure.gateway.user.UserSqlGateway;
import com.desafio.infrastructure.stream.event.UserEvent;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;

import java.time.Duration;

import static com.desafio.shared.utils.Samples.CPF_SAMPLE;
import static com.desafio.shared.utils.Samples.USER_NAME_SAMPLE;
import static org.testcontainers.shaded.org.awaitility.Awaitility.await;

@IntegrationTest
public class EventConsumerIT extends KafkaProducerConfiguration {

  @Autowired
  private KafkaTemplate<String, UserEvent> eventProducer;

  @Autowired
  private UserSqlGateway userSqlGateway;

  @Test
  void whenCompanyEventOfCreateReceived_ShouldAddCompanyToDatabase() {
    UserEvent inputEvent = UserEvent.of(CPF_SAMPLE, USER_NAME_SAMPLE, true);
    eventProducer.send("USER_CREATED", inputEvent);

    final var createUser = userSqlGateway.findByCpf(CPF_SAMPLE);

    await().atMost(Duration.ofSeconds(10)).untilAsserted(() -> Assertions.assertNotNull(createUser));
  }

}

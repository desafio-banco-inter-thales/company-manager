package com.desafio.application.company.create;

import com.desafio.application.UseCaseTest;
import com.desafio.application.company.CompanyDto;
import com.desafio.application.company.ICompanyEntity;
import com.desafio.application.company.ICompanyGateway;
import com.desafio.application.company.find.FindCompanyByTickerOutput;
import com.desafio.application.company.find.FindCompanyByTickerUseCase;
import com.desafio.domain.company.Company;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.List;
import java.util.Objects;

import static com.desafio.shared.utils.ErrorConstants.GATEWAY_INSERT_STR;
import static com.desafio.shared.utils.Samples.*;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class CreateCompanyUseCaseTest extends UseCaseTest {

  @InjectMocks
  private CreateCompanyUseCase createCompanyUseCase;

  @Mock
  private FindCompanyByTickerUseCase findCompanyByTickerUseCase;

  @Mock
  private ICompanyGateway<ICompanyEntity, ICompanyEntity> companyGateway;

  @Override
  protected List<Object> getMocks() {
    return List.of(companyGateway);
  }

  @Test
  void givenAValidCommand_whenCallsCreateCompany_shouldReturnInstance() {
    final var command =
            CreateCompanyCommand.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true);

    when(findCompanyByTickerUseCase.execute(any()))
            .thenReturn(FindCompanyByTickerOutput.of(TICKER_SAMPLE));

    when(companyGateway.save(any()))
            .thenAnswer(returnsFirstArg());

    final var actualOutput = createCompanyUseCase.execute(command);

    Assertions.assertNull(actualOutput.notification());
    Assertions.assertNotNull(actualOutput.id());

    verify(companyGateway, times(1)).save(argThat(company ->
            Objects.equals(COMPANY_NAME_SAMPLE, company.name())
                    && Objects.equals(TICKER_SAMPLE, company.ticker())
                    && Objects.equals(PRICE_SAMPLE, company.price())
                    && Objects.equals(true, company.isActive())
                    && Objects.nonNull(company.id())
                    && Objects.nonNull(company.createdAt())
                    && Objects.nonNull(company.updatedAt())
                    && Objects.isNull(company.deletedAt())
    ));
  }

  @Test
  void givenAValidCommandButAlreadyCreatedCompany_whenCallsCreateCompany_shouldReturnError() {
    final var command =
            CreateCompanyCommand.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true);

    final Company company = Company.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE);
    final CompanyDto companyDto = CompanyDto.of(company);
    when(findCompanyByTickerUseCase.execute(any()))
            .thenReturn(FindCompanyByTickerOutput.of(companyDto));

    final var actualOutput = createCompanyUseCase.execute(command);

    Assertions.assertTrue(actualOutput.notification().hasErrors());
  }

  @Test
  void givenAInvalidNullName_whenCallsCreateCompany_thenShouldReturnListOfErrors() {
    String expectedErrorMessage = "[COMPANY]: 'NAME' SHOULD NOT BE NULL.";
    final var command =
            CreateCompanyCommand.of(null, TICKER_SAMPLE, PRICE_SAMPLE, true);


    final var actualOutput = createCompanyUseCase.execute(command);

    Assertions.assertNotNull(actualOutput.notification());
    Assertions.assertNull(actualOutput.id());
    Assertions.assertEquals(1, actualOutput.notification().getErrors().size());
    Assertions.assertEquals(expectedErrorMessage, actualOutput.notification().messages(""));

    verify(companyGateway, times(0)).save(any());
  }

  @Test
  public void givenAValidCommand_whenGatewayThrowsRandomException_shouldReturnAException() {
    String expectedErrorMessage = "[COMPANY]: ".concat(GATEWAY_INSERT_STR.replace("{}", TICKER_SAMPLE));
    final var command =
            CreateCompanyCommand.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true);

    when(findCompanyByTickerUseCase.execute(any()))
            .thenReturn(FindCompanyByTickerOutput.of(TICKER_SAMPLE));

    when(companyGateway.save(any()))
            .thenThrow(new IllegalStateException(GATEWAY_INSERT_STR));

    final var notification = createCompanyUseCase.execute(command);

    Assertions.assertEquals(1, notification.notification().getErrors().size());
    Assertions.assertEquals(expectedErrorMessage, notification.notification().messages(""));

    verify(companyGateway, times(1)).save(any(ICompanyEntity.class));
  }

}

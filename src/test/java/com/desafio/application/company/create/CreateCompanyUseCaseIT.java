package com.desafio.application.company.create;

import com.desafio.application.company.delete.DeleteCompanyCommand;
import com.desafio.application.company.delete.DeleteCompanyUseCase;
import com.desafio.application.company.find.FindCompanyByTickerCommand;
import com.desafio.application.company.find.FindCompanyByTickerUseCase;
import com.desafio.config.IntegrationTest;
import com.desafio.infrastructure.gateway.company.CompanyRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;

import static com.desafio.shared.utils.Samples.*;

@IntegrationTest
@DirtiesContext
public class CreateCompanyUseCaseIT {

  @Autowired
  CreateCompanyUseCase createCompanyUseCase;

  @Autowired
  FindCompanyByTickerUseCase findCompanyByTickerUseCase;

  @Autowired
  DeleteCompanyUseCase deleteCompanyUseCase;

  @Autowired
  CompanyRepository companyRepository;

  @BeforeEach
  void setUp() {
    deleteCompanyUseCase.execute(DeleteCompanyCommand.of(TICKER_SAMPLE));
  }

  @Test
  void givenAValidCommand_whenCallsUpdateCompany_shouldInsertCompany() {
    Assertions.assertEquals(0, companyRepository.count());

    final var aCommand =
            CreateCompanyCommand.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true);

    final var actualOutput = createCompanyUseCase.execute(aCommand);

    Assertions.assertNotNull(actualOutput);
    Assertions.assertEquals(TICKER_SAMPLE, actualOutput.ticker());

    Assertions.assertEquals(1, companyRepository.count());

    final var output = findCompanyByTickerUseCase.execute(FindCompanyByTickerCommand.of(TICKER_SAMPLE));
    final var actualCompany = output.company();

    Assertions.assertEquals(COMPANY_NAME_SAMPLE, actualCompany.name());
    Assertions.assertEquals(TICKER_SAMPLE, actualCompany.ticker());
    Assertions.assertEquals(PRICE_SAMPLE.setScale(2), actualCompany.price());
    Assertions.assertNotNull(actualCompany.id());
    Assertions.assertNotNull(actualCompany.createdAt());
    Assertions.assertNotNull(actualCompany.updatedAt());
    Assertions.assertNull(actualCompany.deletedAt());
    Assertions.assertTrue(actualCompany.isActive());
  }

}

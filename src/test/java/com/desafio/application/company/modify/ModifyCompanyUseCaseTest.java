package com.desafio.application.company.update;

import com.desafio.application.UseCaseTest;
import com.desafio.application.company.CompanyDto;
import com.desafio.application.company.ICompanyEntity;
import com.desafio.application.company.ICompanyGateway;
import com.desafio.application.company.find.FindCompanyByTickerCommand;
import com.desafio.application.company.find.FindCompanyByTickerOutput;
import com.desafio.application.company.find.FindCompanyByTickerUseCase;
import com.desafio.application.company.modify.ModifyCompanyCommand;
import com.desafio.application.company.modify.ModifyCompanyUseCase;
import com.desafio.application.company.modify.activate.ActivateCompanyUseCase;
import com.desafio.application.company.modify.deactivate.DeactivateCompanyUseCase;
import com.desafio.application.company.modify.update.UpdateCompanyUseCase;
import com.desafio.domain.company.Company;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import static com.desafio.shared.utils.ErrorConstants.*;
import static com.desafio.shared.utils.Samples.*;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class ModifyCompanyUseCaseTest extends UseCaseTest {

  @InjectMocks
  private ModifyCompanyUseCase modifyCompanyUseCase;

  @Mock
  private ICompanyGateway<ICompanyEntity, ICompanyEntity> companyGateway;

  @Mock
  private FindCompanyByTickerUseCase findCompanyByTickerUseCase;

  @Override
  protected List<Object> getMocks() {
    return List.of(companyGateway);
  }

  @Test
  public void givenAValidCommand_whenCallsUpdateCompany_shouldReturnCompany() {
    final var company =
            Company.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE);
    final var companyDto = CompanyDto.of(company);
    final var newPrice = BigDecimal.valueOf(54.32);
    final var updateCommand = ModifyCompanyCommand.of(TICKER_SAMPLE, newPrice);
    final var findCommand = FindCompanyByTickerCommand.of(TICKER_SAMPLE);

    when(findCompanyByTickerUseCase.execute(eq(findCommand)))
            .thenReturn(FindCompanyByTickerOutput.of(companyDto));

    when(companyGateway.save(any()))
            .thenAnswer(returnsFirstArg());

    final var output = modifyCompanyUseCase.execute(updateCommand, UpdateCompanyUseCase.of());

    Assertions.assertNotNull(output);
    Assertions.assertNotNull(output.ticker());

    verify(companyGateway, times(1)).save(argThat(
            aUpdatedCompany ->
                    Objects.equals(COMPANY_NAME_SAMPLE, aUpdatedCompany.name())
                            && Objects.equals(TICKER_SAMPLE, aUpdatedCompany.ticker())
                            && Objects.equals(newPrice, aUpdatedCompany.price())
                            && Objects.nonNull(aUpdatedCompany.createdAt())
                            && Objects.nonNull(aUpdatedCompany.updatedAt())
                            && company.getUpdatedAt().isBefore(aUpdatedCompany.updatedAt())
                            && Objects.isNull(aUpdatedCompany.deletedAt())
    ));
  }

  @Test
  public void givenAInvalidCommand_whenCallsUpdateCompany_shouldReturnNotification() {
    final var newPrice = BigDecimal.valueOf(54.32);
    final var updateCommand = ModifyCompanyCommand.of(TICKER_SAMPLE, newPrice);

    final var output = modifyCompanyUseCase.execute(updateCommand, null);

    Assertions.assertEquals("[COMPANY]: ".concat(YOU_NEED_A_MODIFIER), output.notification().messages(""));
  }

  @Test
  public void givenAInvalidTicker_whenCallsUpdateCompany_thenShouldReturnListOfErrors() {
    final var company =
            Company.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE);
    final var expectedErrorCount = 1;
    final var companyDto = CompanyDto.of(company);
    final var updateCommand = ModifyCompanyCommand.of(null, PRICE_SAMPLE);
    final var findCommand = FindCompanyByTickerCommand.of(null);

    when(findCompanyByTickerUseCase.execute(eq(findCommand)))
            .thenReturn(FindCompanyByTickerOutput.of(companyDto));

    final var output = modifyCompanyUseCase.execute(updateCommand, UpdateCompanyUseCase.of());

    Assertions.assertNotNull(output.notification());
    Assertions.assertEquals(expectedErrorCount, output.notification().getErrors().size());

    verify(companyGateway, times(0)).save(any());
  }

  @Test
  public void givenAValidInactivateCommand_whenCallsUpdateCompany_shouldReturnInactiveCompany() {
    final var company =
            Company.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE);
    final var companyDto = CompanyDto.of(company);
    final var findCommand = FindCompanyByTickerCommand.of(TICKER_SAMPLE);
    final var updateCommand = ModifyCompanyCommand.of(TICKER_SAMPLE);

    when(findCompanyByTickerUseCase.execute(eq(findCommand)))
            .thenReturn(FindCompanyByTickerOutput.of(companyDto));

    when(companyGateway.save(any()))
            .thenAnswer(returnsFirstArg());

    Assertions.assertTrue(company.getActive());
    Assertions.assertNull(company.getDeletedAt());

    final var actualOutput = modifyCompanyUseCase.execute(updateCommand, DeactivateCompanyUseCase.of());

    Assertions.assertNull(actualOutput.notification());
    Assertions.assertNotNull(actualOutput.ticker());
    Assertions.assertFalse(actualOutput.isActive());

    verify(companyGateway, times(1)).save(any());
  }

  @Test
  public void givenAValidActivateCommand_whenCallsUpdateCompany_shouldReturnActiveCompany() {
    final var company =
            Company.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, false);
    final var companyDto = CompanyDto.of(company);
    final var findCommand = FindCompanyByTickerCommand.of(TICKER_SAMPLE);
    final var updateCommand = ModifyCompanyCommand.of(TICKER_SAMPLE);

    when(findCompanyByTickerUseCase.execute(eq(findCommand)))
            .thenReturn(FindCompanyByTickerOutput.of(companyDto));

    when(companyGateway.save(any()))
            .thenAnswer(returnsFirstArg());

    final var actualOutput = modifyCompanyUseCase.execute(updateCommand, ActivateCompanyUseCase.of());

    Assertions.assertNull(actualOutput.notification());
    Assertions.assertNotNull(actualOutput.ticker());
    Assertions.assertTrue(actualOutput.isActive());

    verify(companyGateway, times(1)).save(any());
  }

  @Test
  public void givenAValidCommand_whenGatewayThrowsRandomException_shouldReturnAException() {
    final var company =
            Company.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE);
    final var expectedErrorMessage = "[COMPANY]: ".concat(GATEWAY_UPDATE_STR.replace("{}", company.getTickerStr()));
    final var companyDto = CompanyDto.of(company);
    final var findCommand = FindCompanyByTickerCommand.of(TICKER_SAMPLE);
    final var updateCommand = ModifyCompanyCommand.of(TICKER_SAMPLE, PRICE_SAMPLE);

    when(findCompanyByTickerUseCase.execute(eq(findCommand)))
            .thenReturn(FindCompanyByTickerOutput.of(companyDto));

    when(companyGateway.save(any()))
            .thenThrow(new IllegalStateException(GATEWAY_UPDATE_STR));

    final var output = modifyCompanyUseCase.execute(updateCommand, UpdateCompanyUseCase.of());

    Assertions.assertEquals(1, output.notification().getErrors().size());
    Assertions.assertEquals(expectedErrorMessage, output.notification().messages(""));

    verify(companyGateway, times(1)).save(any());
  }

}
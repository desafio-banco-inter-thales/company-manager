package com.desafio.application.company.modify;

import com.desafio.application.company.create.CreateCompanyCommand;
import com.desafio.application.company.create.CreateCompanyUseCase;
import com.desafio.application.company.delete.DeleteCompanyCommand;
import com.desafio.application.company.delete.DeleteCompanyUseCase;
import com.desafio.application.company.find.FindCompanyByTickerCommand;
import com.desafio.application.company.find.FindCompanyByTickerUseCase;
import com.desafio.application.company.modify.activate.ActivateCompanyUseCase;
import com.desafio.application.company.modify.deactivate.DeactivateCompanyUseCase;
import com.desafio.application.company.modify.update.UpdateCompanyUseCase;
import com.desafio.config.IntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;

import java.math.BigDecimal;

import static com.desafio.shared.utils.Samples.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

@IntegrationTest
@DirtiesContext
public class ModifyCompanyUseCaseIT {

  @Autowired
  CreateCompanyUseCase createCompanyUseCase;

  @Autowired
  ModifyCompanyUseCase modifyCompanyUseCase;

  @Autowired
  FindCompanyByTickerUseCase findCompanyByTickerUseCase;

  @Autowired
  DeleteCompanyUseCase deleteCompanyUseCase;

  @BeforeEach
  void setUp() {
    deleteCompanyUseCase.execute(DeleteCompanyCommand.of(TICKER_SAMPLE));
  }

  @Test
  public void givenAValidCommand_whenCallsUpdateCompany_shouldReturnCompanyFromGateway() {
    final var aCommand =
            CreateCompanyCommand.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true);
    final var newPrice = BigDecimal.valueOf(54.32);
    final var updateCommand = ModifyCompanyCommand.of(TICKER_SAMPLE, newPrice);

    createCompanyUseCase.execute(aCommand);
    modifyCompanyUseCase.execute(updateCommand, UpdateCompanyUseCase.of());

    final var output = findCompanyByTickerUseCase.execute(FindCompanyByTickerCommand.of(TICKER_SAMPLE));
    final var actualCompany = output.company();

    Assertions.assertEquals(COMPANY_NAME_SAMPLE, actualCompany.name());
    Assertions.assertEquals(TICKER_SAMPLE, actualCompany.ticker());
    Assertions.assertEquals(newPrice, actualCompany.price());
    Assertions.assertNotNull(actualCompany.id());
    Assertions.assertNotNull(actualCompany.createdAt());
    Assertions.assertNotNull(actualCompany.updatedAt());
    Assertions.assertNull(actualCompany.deletedAt());
    Assertions.assertTrue(actualCompany.isActive());
  }

  @Test
  public void givenAValidInactivateCommand_whenCallsUpdateCompany_shouldReturnInactiveCompany() {
    final var aCommand =
            CreateCompanyCommand.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true);
    final var updateCommand = ModifyCompanyCommand.of(TICKER_SAMPLE);

    createCompanyUseCase.execute(aCommand);
    modifyCompanyUseCase.execute(updateCommand, DeactivateCompanyUseCase.of());

    final var output = findCompanyByTickerUseCase.execute(FindCompanyByTickerCommand.of(TICKER_SAMPLE));
    final var actualCompany = output.company();

    Assertions.assertFalse(actualCompany.isActive());
  }

  @Test
  public void givenAValidActivateCommand_whenCallsUpdateCompany_shouldReturnActiveCompany() {
    final var aCommand =
            CreateCompanyCommand.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, false);
    final var updateCommand = ModifyCompanyCommand.of(TICKER_SAMPLE);

    createCompanyUseCase.execute(aCommand);
    modifyCompanyUseCase.execute(updateCommand, ActivateCompanyUseCase.of());

    final var output = findCompanyByTickerUseCase.execute(FindCompanyByTickerCommand.of(TICKER_SAMPLE));
    final var actualCompany = output.company();

    Assertions.assertTrue(actualCompany.isActive());
  }

}
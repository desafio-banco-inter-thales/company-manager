package com.desafio.application.company.delete;

import com.desafio.application.company.create.CreateCompanyCommand;
import com.desafio.application.company.create.CreateCompanyUseCase;
import com.desafio.application.company.find.FindCompanyByTickerCommand;
import com.desafio.application.company.find.FindCompanyByTickerUseCase;
import com.desafio.config.IntegrationTest;
import com.desafio.infrastructure.gateway.company.CompanyRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.desafio.shared.utils.Samples.*;

@IntegrationTest
public class DeleteCompanyUseCaseIT {

  @Autowired
  CreateCompanyUseCase createCompanyUseCase;

  @Autowired
  DeleteCompanyUseCase deleteCompanyUseCase;

  @Autowired
  FindCompanyByTickerUseCase findCompanyByTickerUseCase;

  @Autowired
  CompanyRepository companyRepository;

  @BeforeEach
  void setUp() {
    deleteCompanyUseCase.execute(DeleteCompanyCommand.of(TICKER_SAMPLE));
  }

  @Test
  void givenAValidCommand_whenCallsUpdateCompany_shouldInsertCompanyFromGateway() {
    final var findOutput = findCompanyByTickerUseCase.execute(FindCompanyByTickerCommand.of(TICKER_SAMPLE));
    Assertions.assertNull(findOutput.company());

    final var createCompanyCommand =
            CreateCompanyCommand.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true);

    createCompanyUseCase.execute(createCompanyCommand);

    Assertions.assertEquals(1, companyRepository.count());

    deleteCompanyUseCase.execute(DeleteCompanyCommand.of(TICKER_SAMPLE));

    final var output = findCompanyByTickerUseCase.execute(FindCompanyByTickerCommand.of(TICKER_SAMPLE));
    Assertions.assertNull(output.company());
  }

}

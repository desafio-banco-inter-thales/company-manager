package com.desafio.application.company.delete;

import com.desafio.application.UseCaseTest;
import com.desafio.application.company.CompanyDto;
import com.desafio.application.company.ICompanyEntity;
import com.desafio.application.company.ICompanyGateway;
import com.desafio.application.company.find.FindCompanyByTickerOutput;
import com.desafio.application.company.find.FindCompanyByTickerUseCase;
import com.desafio.domain.company.Company;
import com.desafio.infrastructure.gateway.company.CompanyJpaEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.List;

import static com.desafio.shared.utils.Constants.COMPANY_STR;
import static com.desafio.shared.utils.ErrorConstants.GATEWAY_DELETE_STR;
import static com.desafio.shared.utils.Samples.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class DeleteCompanyUseCaseTest extends UseCaseTest {

  @InjectMocks
  private DeleteCompanyUseCase deleteCompanyUseCase;

  @Mock
  private ICompanyGateway<ICompanyEntity, ICompanyEntity> companyGateway;

  @Mock
  private FindCompanyByTickerUseCase findCompanyByTickerUseCase;

  @Override
  protected List<Object> getMocks() {
    return null;
  }

  @Test
  public void givenAValidId_whenCallsDeleteCompany_shouldBeOk() {
    final var company = Company.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE);
    final var companyDto = CompanyDto.of(company, null);
    final var command = DeleteCompanyCommand.of(companyDto.ticker());
    final var findOutput = FindCompanyByTickerOutput.of(companyDto);

    when(findCompanyByTickerUseCase.execute(any()))
            .thenReturn(findOutput);

    doNothing()
            .when(companyGateway).delete(eq(companyDto));

    final var output = deleteCompanyUseCase.execute(command);

    assertNull(output.notification());

    verify(companyGateway, times(1)).delete(eq(companyDto));
  }

  @Test
  public void givenAInvalidTicker_whenCallsDeleteCompany_shouldNotDelete() {
    final var expectedErrorMessage = "[COMPANY]: COMPANY WITH TICKER 'HCTR11' NOT FOUND.";
    final var expectedTicker = "HCTR11";
    final var command = DeleteCompanyCommand.of(expectedTicker);

    when(findCompanyByTickerUseCase.execute(any())).thenReturn(FindCompanyByTickerOutput.of(""));

    final var output = deleteCompanyUseCase.execute(command);

    verify(companyGateway, times(0)).delete(any());
    assertNotNull(output);
    assertEquals(expectedErrorMessage, output.notification().messages(""));
  }

  @Test
  public void givenAValidCommand_whenGatewayThrowsException_shouldReturnException() {
    final var company = Company.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE);
    final var companyDto = CompanyDto.of(company, null);
    final var command = DeleteCompanyCommand.of(companyDto.ticker());
    final var findOutput = FindCompanyByTickerOutput.of(companyDto);
    final var expectedErrorMessage = "[COMPANY]: ".concat(GATEWAY_DELETE_STR.replace("{}", COMPANY_STR));

    when(findCompanyByTickerUseCase.execute(any()))
            .thenReturn(findOutput);

    doThrow(new IllegalStateException(GATEWAY_DELETE_STR))
            .when(companyGateway).delete(eq(companyDto));

    final var output = deleteCompanyUseCase.execute(command);

    Assertions.assertNotNull(output);
    Assertions.assertTrue(output.notification().hasErrors());
    Assertions.assertEquals(1, output.notification().getErrors().size());
    Assertions.assertEquals(expectedErrorMessage, output.notification().messages(""));


    Mockito.verify(companyGateway, times(1)).delete(eq(companyDto));
  }

}

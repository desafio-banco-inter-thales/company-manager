package com.desafio.application.company.list;

import com.desafio.application.company.CompanyDto;
import com.desafio.application.company.create.CreateCompanyCommand;
import com.desafio.application.company.create.CreateCompanyUseCase;
import com.desafio.config.IntegrationTest;
import com.desafio.domain.company.Company;
import com.desafio.infrastructure.api.pagination.SearchQuery;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.List;

import static com.desafio.shared.utils.Samples.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;

@IntegrationTest
public class ListCompaniesUseCaseIT {

  @Autowired
  CreateCompanyUseCase createCompanyUseCase;

  @Autowired
  ListCompaniesUseCase listCompaniesUseCase;

  @Test
  public void givenAValidQuery_whenCallsListCompanies_thenShouldReturnCompaniesFromGateway() {
    final var companies = List.of(
            CompanyDto.of(Company.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE)),
            CompanyDto.of(Company.of("Google", "GOGL11", BigDecimal.valueOf(523.30))),
            CompanyDto.of(Company.of("Google", "GOGL11", BigDecimal.valueOf(523.30), false))
    );

    for (CompanyDto companyDto : companies) {
      createCompanyUseCase.execute(CreateCompanyCommand.of(companyDto.name(), companyDto.ticker(), companyDto.price(), companyDto.isActive()));
    }

    final var expectedPage = 0;
    final var expectedPerPage = 10;

    final var aQuery =
            new SearchQuery(expectedPage, expectedPerPage, true, CPF_SAMPLE);

    final var expectedItemsCount = 2;

    final var actualResult = listCompaniesUseCase.execute(aQuery);

    assertEquals(expectedItemsCount, actualResult.items().size());
    assertEquals(expectedPage, actualResult.currentPage());
    assertEquals(expectedPerPage, actualResult.perPage());
    assertEquals(expectedItemsCount, actualResult.total());
  }

}

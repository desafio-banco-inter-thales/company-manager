package com.desafio.application.company.list;

import com.desafio.application.UseCaseTest;
import com.desafio.application.company.CompanyDto;
import com.desafio.application.company.ICompanyEntity;
import com.desafio.application.company.ICompanyGateway;
import com.desafio.domain.company.Company;
import com.desafio.infrastructure.api.pagination.Pagination;
import com.desafio.infrastructure.api.pagination.SearchQuery;
import com.desafio.infrastructure.api.response.CompanyOutput;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.desafio.shared.utils.Samples.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class ListCompaniesUseCaseTest extends UseCaseTest {

  @InjectMocks
  private ListCompaniesUseCase useCase;

  @Mock
  private ICompanyGateway<ICompanyEntity, CompanyDto> companyGateway;

  @Override
  protected List<Object> getMocks() {
    return List.of(companyGateway);
  }

  @Test
  public void givenAValidQuery_whenCallsListCompanies_thenShouldReturnCompanies() {
    final var companies = List.of(
            CompanyDto.of(Company.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE)),
            CompanyDto.of(Company.of("Google", "GOGL11", BigDecimal.valueOf(523.30)))
    );

    final var expectedPage = 0;
    final var expectedPerPage = 10;

    final var aQuery =
            new SearchQuery(expectedPage, expectedPerPage, true, CPF_SAMPLE);

    final var expectedPagination =
            new Pagination<>(expectedPage, expectedPerPage, companies.size(), companies);

    final var expectedItemsCount = 2;
    final var expectedResult = expectedPagination.map(CompanyOutput::of);

    when(companyGateway.findAll(eq(aQuery)))
            .thenReturn(expectedPagination);

    final var actualResult = useCase.execute(aQuery);

    assertEquals(expectedItemsCount, actualResult.items().size());
    assertEquals(expectedResult, actualResult);
    assertEquals(expectedPage, actualResult.currentPage());
    assertEquals(expectedPerPage, actualResult.perPage());
    assertEquals(companies.size(), actualResult.total());
  }

  @Test
  public void givenAValidQuery_whenHasNoResults_thenShouldReturnEmptyCompanies() {
    final var companies = List.<CompanyDto>of();

    final var expectedPage = 0;
    final var expectedPerPage = 10;

    final var aQuery =
            new SearchQuery(expectedPage, expectedPerPage, true, CPF_SAMPLE);

    final var expectedPagination =
            new Pagination<>(expectedPage, expectedPerPage, companies.size(), companies);

    final var expectedItemsCount = 0;
    final var expectedResult = expectedPagination.map(CompanyOutput::of);

    when(companyGateway.findAll(eq(aQuery)))
            .thenReturn(expectedPagination);

    final var actualResult = useCase.execute(aQuery);

    Assertions.assertEquals(expectedItemsCount, actualResult.items().size());
    Assertions.assertEquals(expectedResult, actualResult);
    Assertions.assertEquals(expectedPage, actualResult.currentPage());
    Assertions.assertEquals(expectedPerPage, actualResult.perPage());
    Assertions.assertEquals(companies.size(), actualResult.total());
  }

  @Test
  public void givenAValidQuery_whenGatewayThrowsException_shouldSuppressException() {
    final var expectedPage = 0;
    final var expectedPerPage = 10;
    final var expectedErrorMessage = "Gateway error";

    final var aQuery =
            new SearchQuery(expectedPage, expectedPerPage, true, CPF_SAMPLE);

    final var expectedPagination =
            new Pagination<>(expectedPage, expectedPerPage, 0, new ArrayList<CompanyDto>());

    final var expectedItemsCount = 0;
    final var expectedResult = expectedPagination.map(CompanyOutput::of);

    when(companyGateway.findAll(eq(aQuery)))
            .thenThrow(new IllegalStateException(expectedErrorMessage));

    final var actualResult = useCase.execute(aQuery);

    Assertions.assertEquals(expectedItemsCount, actualResult.items().size());
    Assertions.assertEquals(expectedResult, actualResult);
    Assertions.assertEquals(expectedPage, actualResult.currentPage());
    Assertions.assertEquals(expectedPerPage, actualResult.perPage());
    Assertions.assertEquals(0, actualResult.total());
  }

}

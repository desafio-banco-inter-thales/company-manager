package com.desafio.application.company.find;

import com.desafio.application.UseCaseTest;
import com.desafio.application.company.CompanyDto;
import com.desafio.application.company.ICompanyEntity;
import com.desafio.application.company.ICompanyGateway;
import com.desafio.domain.company.Company;
import com.desafio.domain.company.ticker.Ticker;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.List;
import java.util.Optional;

import static com.desafio.shared.utils.ErrorConstants.GATEWAY_FIND_STR;
import static com.desafio.shared.utils.Samples.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class FindCompanyByTickerUseCaseTest extends UseCaseTest {

  @InjectMocks
  private FindCompanyByTickerUseCase useCase;

  @Mock
  private ICompanyGateway<ICompanyEntity, ICompanyEntity> companyGateway;

  @Override
  protected List<Object> getMocks() {
    return null;
  }

  @Test
  public void givenAValidId_whenCallsGetCompany_shouldReturnCompany() {
    final var company = Company.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE);
    final var companyDto = CompanyDto.of(company, null);
    final var command = FindCompanyByTickerCommand.of(TICKER_SAMPLE);

    when(companyGateway.findByTicker(TICKER_SAMPLE))
            .thenReturn(Optional.of(companyDto));

    final var output = useCase.execute(command);
    final var actualCompany = output.company();

    Assertions.assertEquals(company.getId().getValue(), actualCompany.id());
    Assertions.assertEquals(COMPANY_NAME_SAMPLE, actualCompany.name());
    Assertions.assertEquals(PRICE_SAMPLE, actualCompany.price());
    Assertions.assertEquals(TICKER_SAMPLE, actualCompany.ticker());
    Assertions.assertEquals(true, actualCompany.isActive());
    Assertions.assertNotNull(actualCompany.createdAt());
    Assertions.assertNotNull(actualCompany.updatedAt());
    Assertions.assertNull(actualCompany.deletedAt());
  }

  @Test
  public void givenAInvalidId_whenCallsGetCompany_shouldReturnNotFound() {
    final var expectedErrorMessage = "[COMPANY]: COMPANY WITH TICKER '123' NOT FOUND.";
    final var expectedTicker = Ticker.of("123", PRICE_SAMPLE);

    when(companyGateway.findByTicker(eq(expectedTicker.getValue())))
            .thenReturn(Optional.empty());

    final var command = FindCompanyByTickerCommand.of(expectedTicker.getValue());
    final var output = useCase.execute(command);

    Assertions.assertEquals(expectedErrorMessage, output.notificationErrors().messages(""));
  }

  @Test
  public void givenAValidId_whenGatewayThrowsException_shouldReturnException() {
    final var expectedTicker = Ticker.of("123", PRICE_SAMPLE);
    final var expectedErrorMessage = "[COMPANY]: COULD NOT FIND OBJECT {}".replace("{}", expectedTicker.getValue());

    when(companyGateway.findByTicker(eq(expectedTicker.getValue())))
            .thenThrow(new IllegalStateException(GATEWAY_FIND_STR));

    final var command = FindCompanyByTickerCommand.of(expectedTicker.getValue());
    final var output = useCase.execute(command);

    Assertions.assertEquals(expectedErrorMessage, output.notificationErrors().messages(""));
  }
}

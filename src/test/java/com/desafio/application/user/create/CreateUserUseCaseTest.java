package com.desafio.application.user.create;

import com.desafio.application.user.IUserGateway;
import com.desafio.application.user.UserDto;
import com.desafio.application.user.find.FindUserByCpfOutput;
import com.desafio.application.user.find.FindUserByCpfUseCase;
import com.desafio.domain.user.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Objects;

import static com.desafio.shared.utils.ErrorConstants.GATEWAY_INSERT_STR;
import static com.desafio.shared.utils.ErrorConstants.USER_ALREADY_EXISTS;
import static com.desafio.shared.utils.Samples.CPF_SAMPLE;
import static com.desafio.shared.utils.Samples.USER_NAME_SAMPLE;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CreateUserUseCaseTest {

  @InjectMocks
  private CreateUserUseCase createUserUseCase;

  @Mock
  private IUserGateway userGateway;

  @Mock
  private FindUserByCpfUseCase findUserByCpfUseCase;

  @Test
  void givenValidCommand_whenCallsCreateCategory_shouldReturnCategoryId() {
    final var command = CreateUserCommand.of(USER_NAME_SAMPLE, CPF_SAMPLE, true);

    when(findUserByCpfUseCase.execute(any()))
            .thenReturn(FindUserByCpfOutput.of(""));

    final CreateUserOutput output = createUserUseCase.execute(command);

    Assertions.assertNotNull(output);
    Assertions.assertNotNull(output.cpf());

    verify(userGateway, times(1)).save(argThat(user ->
            Objects.equals(USER_NAME_SAMPLE, user.name()) && Objects.equals(CPF_SAMPLE, user.cpf())
    ));
  }

  @Test
  void givenValidCommandAndExistentUser_whenCallsCreateCategory_shouldReturnCategoryId() {
    final var command = CreateUserCommand.of(USER_NAME_SAMPLE, CPF_SAMPLE, true);
    final var expectedMessage = "[USER]: ".concat(USER_ALREADY_EXISTS.replace("{}", CPF_SAMPLE));
    final var user = User.of(USER_NAME_SAMPLE, CPF_SAMPLE);

    when(findUserByCpfUseCase.execute(any()))
            .thenReturn(FindUserByCpfOutput.of(UserDto.of(user)));

    final var createUser = createUserUseCase.execute(command);

    Assertions.assertNotNull(createUser.notificationErrors());
    Assertions.assertEquals(createUser.notificationErrors().messages(""), expectedMessage);

    verify(findUserByCpfUseCase, times(1)).execute(any());
  }

  @Test
  void givenInvalidBlankName_whenCallsCreateCategory_shouldThrowAnError() {
    final String expected_error_message = "[USER]: 'NAME' SHOULD NOT BE BLANK.[USER]: 'NAME' MUST BE BETWEEN 3 AND 30 CHARACTERS.";

    final var command = CreateUserCommand.of("", CPF_SAMPLE, true);
    final var output = createUserUseCase.execute(command);

    Assertions.assertEquals(expected_error_message, output.notificationErrors().messages(""));
    verify(userGateway, times(0)).save(any());
  }

  @Test
  void givenValidCommand_whenGatewayThrowsAnError_shouldThrowAnError() {
    final var command = CreateUserCommand.of(USER_NAME_SAMPLE, CPF_SAMPLE, true);
    String expected_error_message = "[USER]: COULD NOT INSERT OBJECT {}".replace("{}", command.cpf());

    when(findUserByCpfUseCase.execute(any()))
            .thenReturn(FindUserByCpfOutput.of(""));

    when(userGateway.save(any())).thenThrow(new RuntimeException(GATEWAY_INSERT_STR));

    CreateUserOutput output = createUserUseCase.execute(command);

    Assertions.assertEquals(expected_error_message, output.notificationErrors().messages(""));

    verify(userGateway, times(1)).save(any());
  }
}

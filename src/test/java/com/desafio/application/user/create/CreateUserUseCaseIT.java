package com.desafio.application.user.create;

import com.desafio.application.user.find.FindUserByCpfCommand;
import com.desafio.application.user.find.FindUserByCpfUseCase;
import com.desafio.config.IntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.desafio.shared.utils.Samples.CPF_SAMPLE;
import static com.desafio.shared.utils.Samples.USER_NAME_SAMPLE;

@IntegrationTest
public class CreateUserUseCaseIT {

  @Autowired
  CreateUserUseCase createUserUseCase;

  @Autowired
  FindUserByCpfUseCase findUserByCpfUseCase;

  @Test
  void givenValidCommand_whenCallsCreateCategory_shouldReturnCategoryId() {
    final var command = CreateUserCommand.of(USER_NAME_SAMPLE, CPF_SAMPLE, true);

    createUserUseCase.execute(command);

    final var output = findUserByCpfUseCase.execute(FindUserByCpfCommand.of(CPF_SAMPLE));

    Assertions.assertEquals(command.cpf(), output.cpf());
    Assertions.assertEquals(command.isAdm(), output.isAdm());
    Assertions.assertEquals(command.name(), output.name());
  }
}

CREATE SCHEMA IF NOT EXISTS desafio;

CREATE TABLE IF NOT EXISTS `company`(
    id VARCHAR(36) NOT NULL PRIMARY KEY,
    name VARCHAR(30) NOT NULL,
    ticker VARCHAR(7) NOT NULL,
    price DECIMAL(10, 2),
    is_active BIT DEFAULT 1,
    created_at DATETIME(6) NOT NULL,
    updated_at DATETIME(6) NOT NULL,
    deleted_at DATETIME(6)
);

CREATE TABLE IF NOT EXISTS `user`(
    id VARCHAR(36) NOT NULL PRIMARY KEY,
    cpf CHAR(11) NOT NULL,
    name VARCHAR(30) NOT NULL,
    is_adm BIT DEFAULT 1
);

--INSERT INTO `user` (id, cpf, name, is_adm) VALUES('1', '05317350107', 'Thales', true);
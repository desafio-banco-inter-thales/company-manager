package com.desafio.domain.user;

import com.desafio.domain.Aggregate;
import com.desafio.domain.user.afterValidation.IAfterValidation;
import com.desafio.domain.user.afterValidation.formatCpf.FormatCpf;
import com.desafio.domain.user.cpf.Cpf;
import com.desafio.domain.user.cpf.CpfValidator;
import com.desafio.domain.user.cpf.ICpf;
import com.desafio.shared.notification.INotification;
import com.desafio.shared.notification.Notification;

import java.util.List;

public class User extends Aggregate<UserId> implements IUser {

  private final String name;
  private ICpf cpf;

  private final Boolean isAdm;

  protected User(String id, ICpf cpf, String name, Boolean isAdm, INotification notification) {
    super(id == null? UserId.unique() : UserId.from(id), notification);
    this.cpf = cpf;
    this.name = name;
    this.isAdm = isAdm;
  }

  public static User of(String name, String cpf) {
    INotification notification = new Notification();
    Cpf cpfObj = Cpf.of(cpf);
    return new User(null, cpfObj, name, false, notification);
  }

  public static User of(String name, String cpf, Boolean isAdm) {
    INotification notification = new Notification();
    Cpf cpfObj = Cpf.of(cpf);
    return new User(null, cpfObj, name, isAdm, notification);
  }

  public static User of(String id, String name, String cpf, Boolean isAdm) {
    INotification notification = new Notification();
    Cpf cpfObj = Cpf.of(cpf);
    return new User(id, cpfObj, name, isAdm, notification);
  }


  public ICpf getCpf() {
    return cpf;
  }

  @Override
  public String getIdStr() {
    return id.getValue();
  }

  @Override
  public String getCpfStr() {
    return cpf.getValue();
  }

  public String getName() {
    return name;
  }

  public Boolean getAdm() {
    return isAdm;
  }

  public void formatCpf() {
    this.cpf = Cpf.format(this.cpf.getValue());
  }

  @Override
  public void validate() {
    List<IAfterValidation<User>> validationHandlers = List.of(new FormatCpf());
    new UserValidator(new CpfValidator(), validationHandlers).validate(this);
  }

  @Override
  public String toString() {
    return "User{" +
            "name='" + name + '\'' +
            ", cpf=" + cpf +
            ", isAdm=" + isAdm +
            ", id=" + id +
            '}';
  }
}

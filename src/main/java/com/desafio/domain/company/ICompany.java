package com.desafio.domain.company;

import com.desafio.shared.notification.INotification;

import java.math.BigDecimal;
import java.time.Instant;

public interface ICompany {
  String getIdStr();
  String getTickerStr();
  String getName();
  BigDecimal getPrice();
  Boolean getActive();
  INotification getNotification();
  Instant getCreatedAt();
  Instant getUpdatedAt();
  Instant getDeletedAt();
}

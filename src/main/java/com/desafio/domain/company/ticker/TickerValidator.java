package com.desafio.domain.company.ticker;



import com.desafio.shared.notification.INotification;

import java.math.BigDecimal;

import static com.desafio.shared.utils.Constants.*;
import static com.desafio.shared.utils.ErrorConstants.*;

public class TickerValidator implements ITickerValidator {

  @Override
  public void validate(ITicker ticker, INotification notification) {
    checkTickerName(ticker, notification);
    checkPrice(ticker, notification);
  }

  private void checkTickerName(ITicker ticker, INotification notification) {
    if (ticker.getValue() == null) {
      notification.append(STRING_SHOULD_NOT_BE_NULL.replace("{}", TICKER_STR), COMPANY_STR);
      return;
    } else if (ticker.getValue().isEmpty()) {
      notification.append(STRING_SHOULD_NOT_BE_BLANK.replace("{}", TICKER_STR), COMPANY_STR);
    }

    final String ticker_str = ticker.getValue().trim();
    if ((ticker_str.length() < MIN_TICKER_LEN) || (ticker_str.length() > MAX_TICKER_LEN)) {
      notification.append(TICKER_LENGTH_INVALID, COMPANY_STR);
    }
  }

  private void checkPrice(ITicker ticker, INotification notification) {
    if (ticker == null)
      return;
    else if (ticker.getPrice() == null){
      notification.append(String.format(STRING_SHOULD_NOT_BE_NULL, PRICE_STR), COMPANY_STR);
      return;
    } else if (ticker.getPrice().compareTo(BigDecimal.valueOf(0)) < 0)
      notification.append(String.format(PRICE_INVALID, PRICE_STR), COMPANY_STR);
  }

}

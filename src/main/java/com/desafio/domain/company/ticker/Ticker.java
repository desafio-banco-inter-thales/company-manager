package com.desafio.domain.company.ticker;

import com.desafio.domain.ValueObject;

import java.math.BigDecimal;
import java.util.Objects;

public class Ticker extends ValueObject implements ITicker {
  private final String value;
  private final BigDecimal price;

  private Ticker(String value, BigDecimal price) {
    this.value = value;
    this.price = price;
  }

  public static Ticker of(String value, BigDecimal price) {
    return new Ticker(value, price);
  }

  @Override
  public String getValue() {
    return value;
  }

  @Override
  public BigDecimal getPrice() {
    return price;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Ticker ticker = (Ticker) o;
    return getValue().equals(ticker.getValue());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getValue());
  }

  @Override
  public String toString() {
    return "Ticker{" +
            "value='" + value + '\'' +
            ", price=" + price +
            '}';
  }
}
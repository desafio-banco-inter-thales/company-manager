package com.desafio.domain.company.ticker;

import java.math.BigDecimal;

public interface ITicker {

  public String getValue();
  public BigDecimal getPrice();

}

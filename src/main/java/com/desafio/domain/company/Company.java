package com.desafio.domain.company;

import com.desafio.domain.Aggregate;
import com.desafio.domain.company.ticker.ITicker;
import com.desafio.domain.company.ticker.Ticker;
import com.desafio.domain.company.ticker.TickerValidator;
import com.desafio.shared.notification.INotification;
import com.desafio.shared.notification.Notification;

import java.math.BigDecimal;
import java.time.Instant;

public class Company extends Aggregate<CompanyId> implements ICompany {

  private String name;
  private ITicker ticker;
  private Boolean isActive;
  private Instant createdAt;
  private Instant updatedAt;
  private Instant deletedAt;

  protected Company(String id, String name, ITicker ticker, Boolean isActive, Instant createdAt, Instant updatedAt, Instant deletedAt, INotification notification) {
    super(id == null ? CompanyId.unique() : CompanyId.from(id), notification);
    this.name = name;
    this.isActive = isActive;
    this.ticker = ticker;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
  }

  public Company update(String ticker, BigDecimal price) {
    this.ticker = Ticker.of(ticker, price);
    this.updatedAt = Instant.now();
    return this;
  }

  public Company activate() {
    this.deletedAt = null;
    this.isActive = true;
    this.updatedAt = Instant.now();
    return this;
  }

  public Company deactivate() {
    if (getDeletedAt() == null) {
      this.deletedAt = Instant.now();
    }

    this.isActive = false;
    this.updatedAt = Instant.now();
    return this;
  }

  public static Company of(String name, String ticker, BigDecimal price) {
    INotification notification = new Notification();
    ITicker tickerObj = Ticker.of(ticker, price);
    Instant now = Instant.now();
    return new Company(null, name, tickerObj, true, now, now, null, notification);
  }

  public static Company of(String id, String name, String ticker, BigDecimal price, Boolean isActive) {
    INotification notification = new Notification();
    ITicker tickerObj = Ticker.of(ticker, price);
    Instant now = Instant.now();
    return new Company(id, name, tickerObj, isActive, now, now, null, notification);
  }

  public static Company of(String name, String ticker, BigDecimal price, Boolean isActive) {
    INotification notification = new Notification();
    ITicker tickerObj = Ticker.of(ticker, price);
    Instant now = Instant.now();
    return new Company(null, name, tickerObj, isActive, now, now, null, notification);
  }

  @Override
  public void validate() {
    new CompanyValidator(new TickerValidator()).validate(this);
  }

  @Override
  public String getIdStr() {
    return this.id.getValue();
  }

  @Override
  public String getTickerStr() {
    return this.getTicker().getValue();
  }

  public String getName() {
    return name;
  }

  @Override
  public BigDecimal getPrice() {
    return this.getTicker().getPrice();
  }

  public ITicker getTicker() {
    return ticker;
  }

  public Boolean getActive() {
    return isActive;
  }

  public Instant getCreatedAt() {
    return createdAt;
  }

  public Instant getUpdatedAt() {
    return updatedAt;
  }

  public Instant getDeletedAt() {
    return deletedAt;
  }

  @Override
  public String toString() {
    return "Company{" +
            "name='" + name + '\'' +
            ", ticker=" + ticker +
            ", isActive=" + isActive +
            ", createdAt=" + createdAt +
            ", updatedAt=" + updatedAt +
            ", deletedAt=" + deletedAt +
            ", id=" + id +
            '}';
  }
}

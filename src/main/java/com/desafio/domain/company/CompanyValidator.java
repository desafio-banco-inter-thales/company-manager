package com.desafio.domain.company;

import com.desafio.domain.Validator;
import com.desafio.domain.company.ticker.ITickerValidator;

import static com.desafio.shared.utils.Constants.*;
import static com.desafio.shared.utils.ErrorConstants.*;

public class CompanyValidator extends Validator<Company> {

  private ITickerValidator iTickerValidator;

  public CompanyValidator(ITickerValidator iTickerValidator) {
    this.iTickerValidator = iTickerValidator;
  }

  @Override
  public void validate(Company company) {
    checkName(company);
    this.iTickerValidator.validate(company.getTicker(), company.getNotification());
  }

  private void checkName(Company company) {
    if (company.getName() == null) {
      company.getNotification().append(STRING_SHOULD_NOT_BE_NULL.replace("{}", NAME_STR), COMPANY_STR);
      return;
    } else if (company.getName().isEmpty()) {
      company.getNotification().append(STRING_SHOULD_NOT_BE_BLANK.replace("{}", NAME_STR), COMPANY_STR);
    }

    final String name = company.getName().trim();
    if ((name.length() < MIN_NAME_LEN) || (name.length() > MAX_NAME_LEN)) {
      company.getNotification().append(NAME_LENGTH_INVALID, COMPANY_STR);
    }
  }
}

package com.desafio.domain.company;

import com.desafio.domain.Identifier;

import java.util.Objects;
import java.util.UUID;

public class CompanyId extends Identifier {

  private final String value;

  public CompanyId(String value) {
    this.value = Objects.requireNonNull(value);
  }

  public static CompanyId unique() {
    return CompanyId.from(UUID.randomUUID());
  }

  public static CompanyId from(String id) {
    return new CompanyId(id);
  }

  public static CompanyId from(UUID id) {
    return new CompanyId(id.toString().toLowerCase());
  }

  public String getValue() {
    return value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CompanyId companyId = (CompanyId) o;
    return getValue().equals(companyId.getValue());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getValue());
  }

  @Override
  public String toString() {
    return "CompanyId{" +
            "value='" + value + '\'' +
            '}';
  }
}

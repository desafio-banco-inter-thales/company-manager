package com.desafio.application.user.find;

import com.desafio.application.user.IUserEntity;
import com.desafio.shared.notification.INotification;
import com.desafio.shared.notification.Notification;
import com.desafio.shared.notification.NotificationErrorProps;

import static com.desafio.shared.utils.Constants.USER_STR;

public record FindUserByCpfOutput(String name, String cpf, Boolean isAdm, INotification notificationErrors) {

  public static FindUserByCpfOutput of(IUserEntity user) {
    return new FindUserByCpfOutput(user.name(), user.cpf(), user.isAdm(), null);
  }

  public static FindUserByCpfOutput of(INotification notification) {
    return new FindUserByCpfOutput(null, null, null, notification);
  }

  public static FindUserByCpfOutput of(String message) {
    Notification notification = new Notification();
    NotificationErrorProps userAlreadyExists = new NotificationErrorProps(message, USER_STR);
    notification.append(userAlreadyExists);
    return new FindUserByCpfOutput(null, null, null, notification);
  }
}
package com.desafio.application.user;

import com.desafio.domain.user.IUser;
import com.desafio.domain.user.User;
import com.desafio.shared.notification.INotification;

import java.math.BigDecimal;

public record UserDto(String id, String cpf, Boolean isAdm, String name, INotification notification) implements IUserEntity {

  public static UserDto of(String id, String cpf, Boolean isAdm, String name, INotification notification) {
    return new UserDto(id, cpf, isAdm, name, notification);
  }

  public static UserDto of(String id, String cpf, String name, Boolean isAdm) {
    return new UserDto(id, cpf, isAdm, name, null);
  }

  public static UserDto of(IUser iUser) {
    return new UserDto(iUser.getIdStr(), iUser.getCpfStr(), iUser.getAdm(), iUser.getName(), iUser.getNotification());
  }

  public static UserDto of(IUserEntity iUserEntity) {
    return new UserDto(iUserEntity.id(), iUserEntity.cpf(), iUserEntity.isAdm(), iUserEntity.name(), null);
  }
}

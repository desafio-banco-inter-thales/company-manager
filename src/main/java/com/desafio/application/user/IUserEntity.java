package com.desafio.application.user;

public interface IUserEntity {
  String name();
  String cpf();
  String id();
  Boolean isAdm();

  String toString();
}

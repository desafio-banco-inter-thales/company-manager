package com.desafio.application.user.adm;

import com.desafio.application.user.IUserEntity;
import com.desafio.shared.notification.INotification;
import com.desafio.shared.notification.Notification;
import com.desafio.shared.notification.NotificationErrorProps;

import static com.desafio.shared.utils.Constants.USER_STR;

public record FindAdmUserByCpfOutput(String name, String cpf, Boolean isAdm, INotification notificationErrors) {

  public static FindAdmUserByCpfOutput of(IUserEntity user) {
    return new FindAdmUserByCpfOutput(user.name(), user.cpf(), user.isAdm(), null);
  }

  public static FindAdmUserByCpfOutput of(INotification notification) {
    return new FindAdmUserByCpfOutput(null, null, null, notification);
  }

  public static FindAdmUserByCpfOutput of(String message) {
    Notification notification = new Notification();
    NotificationErrorProps userAlreadyExists = new NotificationErrorProps(message, USER_STR);
    notification.append(userAlreadyExists);
    return new FindAdmUserByCpfOutput(null, null, null, notification);
  }
}
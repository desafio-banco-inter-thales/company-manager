package com.desafio.application.user.adm;

import com.desafio.application.user.IUserEntity;
import com.desafio.application.user.IUserGateway;
import com.desafio.domain.user.cpf.CpfValidator;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;

import java.util.Optional;

import static com.desafio.shared.utils.ActionConstants.FIND_ADM_BY_CPF;
import static com.desafio.shared.utils.ErrorConstants.*;
import static com.desafio.shared.utils.SuccessConstants.ADM_USER_FOUND;

public class FindAdmUserByCpfUseCase {

  private static final ILog log = new Log(FindAdmUserByCpfUseCase.class);

  private final IUserGateway userGateway;

  private FindAdmUserByCpfUseCase(IUserGateway userGateway) {
    this.userGateway = userGateway;
  }

  public static FindAdmUserByCpfUseCase of(IUserGateway gateway) {
    return new FindAdmUserByCpfUseCase(gateway);
  }

  public FindAdmUserByCpfOutput execute(FindAdmUserByCpfCommand command) {
    log.info(FIND_ADM_BY_CPF, command.cpf());

    if (!CpfValidator.isCPF(command.cpf()))
      return FindAdmUserByCpfOutput.of(CPF_INVALID.replace("{}", command.cpf()));

    try {
      Optional<IUserEntity> actualUser = this.userGateway.findAdmByCpf(command.cpf());

      if(actualUser.isEmpty()) {
        log.info(ADM_USER_DOES_NOT_EXISTS, command.cpf());
        return FindAdmUserByCpfOutput.of(ADM_USER_DOES_NOT_EXISTS.replace("{}", command.cpf()));
      }

      log.info(ADM_USER_FOUND, actualUser.toString());
      return FindAdmUserByCpfOutput.of(actualUser.get());

    } catch (Exception e) {
      return FindAdmUserByCpfOutput.of(GATEWAY_FIND_STR.replace("{}", command.cpf()));
    }
  }

}

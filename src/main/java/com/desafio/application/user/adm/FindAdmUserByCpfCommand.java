package com.desafio.application.user.adm;

public record FindAdmUserByCpfCommand(String cpf) {

  public static FindAdmUserByCpfCommand of(String cpf) {
    return new FindAdmUserByCpfCommand(cpf);
  }

}

package com.desafio.application.user;

import java.util.Optional;

public interface IUserGateway {
  Optional<IUserEntity> findByCpf(String cpf);
  Optional<IUserEntity> findAdmByCpf(String cpf);
  IUserEntity save(IUserEntity user);
}

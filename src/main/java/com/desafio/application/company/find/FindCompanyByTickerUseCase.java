package com.desafio.application.company.find;

import com.desafio.application.company.ICompanyEntity;
import com.desafio.application.company.ICompanyGateway;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;

import java.util.Optional;

import static com.desafio.shared.utils.ActionConstants.DELETE_COMPANY;
import static com.desafio.shared.utils.ActionConstants.FIND_COMPANY;
import static com.desafio.shared.utils.ErrorConstants.COMPANY_DOES_NOT_EXISTS;
import static com.desafio.shared.utils.ErrorConstants.GATEWAY_FIND_STR;
import static com.desafio.shared.utils.SuccessConstants.COMPANY_FOUND;

public class FindCompanyByTickerUseCase {

  private static final ILog log = new Log(FindCompanyByTickerUseCase.class);

  private final ICompanyGateway<ICompanyEntity, ICompanyEntity> companyGateway;

  private FindCompanyByTickerUseCase(ICompanyGateway<ICompanyEntity, ICompanyEntity> gateway) {
    this.companyGateway = gateway;
  }

  public static FindCompanyByTickerUseCase of(ICompanyGateway gateway) {
    return new FindCompanyByTickerUseCase(gateway);
  }

  public FindCompanyByTickerOutput execute(FindCompanyByTickerCommand command) {
    log.info(FIND_COMPANY, command.ticker());
    Optional<ICompanyEntity> company = Optional.empty();
    try {
       company = this.companyGateway.findByTicker(command.ticker());
    } catch (Exception e) {
      log.error(e.getMessage());
      return FindCompanyByTickerOutput.of(GATEWAY_FIND_STR.replace("{}", command.ticker()));
    }


    if (company.isEmpty()) {
      String message = COMPANY_DOES_NOT_EXISTS.replace("{}", command.ticker());
      log.info(message);
      return FindCompanyByTickerOutput.of(message);
    }

    log.info(COMPANY_FOUND, company.get().toString());
    return FindCompanyByTickerOutput.of(company.get());
  }
}

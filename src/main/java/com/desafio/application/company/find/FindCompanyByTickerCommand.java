package com.desafio.application.company.find;

public record FindCompanyByTickerCommand(String ticker) {
  public static FindCompanyByTickerCommand of(String ticker) {
    return new FindCompanyByTickerCommand(ticker);
  }
}

package com.desafio.application.company;

import com.desafio.domain.company.Company;
import com.desafio.domain.company.ICompany;
import com.desafio.shared.notification.INotification;

import java.math.BigDecimal;
import java.time.Instant;

public record CompanyDto(String id, String name, String ticker, BigDecimal price, Boolean isActive, Instant createdAt, Instant updatedAt, Instant deletedAt, INotification notification) implements ICompanyEntity {

  public static CompanyDto of(ICompany iCompany) {
    return new CompanyDto(iCompany.getIdStr(), iCompany.getName(), iCompany.getTickerStr(), iCompany.getPrice(), iCompany.getActive(), iCompany.getCreatedAt(), iCompany.getUpdatedAt(), iCompany.getDeletedAt(), iCompany.getNotification());
  }

  public static CompanyDto of(ICompany iCompany, INotification notification) {
    return new CompanyDto(iCompany.getIdStr(), iCompany.getName(), iCompany.getTickerStr(), iCompany.getPrice(), iCompany.getActive(), iCompany.getCreatedAt(), iCompany.getUpdatedAt(), iCompany.getDeletedAt(), notification);
  }

  public static CompanyDto of(String id, String name, String ticker, BigDecimal price, Boolean isActive) {
    return new CompanyDto(id, name, ticker, price, isActive, null, null, null, null);
  }

  public static CompanyDto of(String name, String ticker, BigDecimal price, Boolean isActive) {
    return new CompanyDto(null, name, ticker, price, isActive, null, null, null, null);
  }

  public static CompanyDto of(ICompanyEntity iCompanyEntity) {
    return new CompanyDto(iCompanyEntity.id(), iCompanyEntity.name(), iCompanyEntity.ticker(), iCompanyEntity.price(), iCompanyEntity.isActive(), iCompanyEntity.createdAt(), iCompanyEntity.updatedAt(), iCompanyEntity.deletedAt(), null);
  }

  public ICompany toAggregate() {
    return Company.of(id, name, ticker, price, isActive);
  }

  public String toStringCreation() {
    return "CompanyDto{" +
            ", name='" + name + '\'' +
            ", ticker='" + ticker + '\'' +
            ", price=" + price +
            ", isActive=" + isActive +
            '}';
  }
}

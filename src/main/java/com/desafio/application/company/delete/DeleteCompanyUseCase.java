package com.desafio.application.company.delete;

import com.desafio.application.company.CompanyDto;
import com.desafio.application.company.ICompanyEntity;
import com.desafio.application.company.ICompanyGateway;
import com.desafio.application.company.find.FindCompanyByTickerCommand;
import com.desafio.application.company.find.FindCompanyByTickerOutput;
import com.desafio.application.company.find.FindCompanyByTickerUseCase;
import com.desafio.domain.company.Company;
import com.desafio.infrastructure.gateway.company.CompanyJpaEntity;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;

import java.util.Optional;

import static com.desafio.shared.utils.ActionConstants.CREATE_COMPANY;
import static com.desafio.shared.utils.ActionConstants.DELETE_COMPANY;
import static com.desafio.shared.utils.Constants.COMPANY_STR;
import static com.desafio.shared.utils.ErrorConstants.*;
import static com.desafio.shared.utils.SuccessConstants.COMPANY_INSERTED_ON_GATEWAY;
import static com.desafio.shared.utils.SuccessConstants.COMPANY_REMOVED_ON_GATEWAY;

public class DeleteCompanyUseCase {

  private static final ILog log = new Log(DeleteCompanyUseCase.class);

  private final ICompanyGateway<ICompanyEntity, ICompanyEntity> companyGateway;

  private final FindCompanyByTickerUseCase findCompanyByTickerUseCase;

  private DeleteCompanyUseCase(
          ICompanyGateway<ICompanyEntity, ICompanyEntity> gateway,
          FindCompanyByTickerUseCase findCompanyByTickerUseCase) {
    this.companyGateway = gateway;
    this.findCompanyByTickerUseCase = findCompanyByTickerUseCase;
  }

  public static DeleteCompanyUseCase of(ICompanyGateway gateway, FindCompanyByTickerUseCase findCompanyByTickerUseCase) {
    return new DeleteCompanyUseCase(gateway, findCompanyByTickerUseCase);
  }

  public DeleteCompanyOutput execute(DeleteCompanyCommand command) {
    log.info(DELETE_COMPANY, command.ticker());
    try {
      FindCompanyByTickerOutput output = this.findCompanyByTickerUseCase.execute(FindCompanyByTickerCommand.of(command.ticker()));

      if(output.notificationErrors() != null) {
        log.info(COMPANY_NOT_DELETED_BC_IT_DOES_NOT_EXIST, command.ticker());
        return DeleteCompanyOutput.of(COMPANY_DOES_NOT_EXISTS.replace("{}", command.ticker()));
      }

      this.companyGateway.delete(CompanyDto.of(output.company()));
      log.info(COMPANY_REMOVED_ON_GATEWAY, command.ticker());
      return DeleteCompanyOutput.of(output.company());
    } catch (Exception e) {
      log.error(e.getMessage());
      return DeleteCompanyOutput.of(GATEWAY_DELETE_STR.replace("{}", COMPANY_STR));
    }
  }
}

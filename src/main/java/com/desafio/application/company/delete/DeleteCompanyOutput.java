package com.desafio.application.company.delete;


import com.desafio.application.company.ICompanyEntity;
import com.desafio.shared.notification.INotification;
import com.desafio.shared.notification.Notification;

import static com.desafio.shared.utils.Constants.COMPANY_STR;

public record DeleteCompanyOutput(ICompanyEntity company, INotification notification) {

  public static DeleteCompanyOutput of(String message) {
    INotification notification = new Notification();
    notification.append(message, COMPANY_STR);
    return new DeleteCompanyOutput(null, notification);
  }

  public static DeleteCompanyOutput of(ICompanyEntity company) {
    return new DeleteCompanyOutput(company, null);
  }
}

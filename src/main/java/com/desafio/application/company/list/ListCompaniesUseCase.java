package com.desafio.application.company.list;

import com.desafio.application.company.ICompanyEntity;
import com.desafio.application.company.ICompanyGateway;
import com.desafio.infrastructure.api.pagination.Pagination;
import com.desafio.infrastructure.api.pagination.SearchQuery;
import com.desafio.infrastructure.api.response.CompanyOutput;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;

import java.util.ArrayList;

import static com.desafio.shared.utils.ActionConstants.FIND_ALL_COMPANIES;
import static com.desafio.shared.utils.SuccessConstants.FOUND_X_COMPANIES;

public class ListCompaniesUseCase {

  private final ICompanyGateway<ICompanyEntity, ICompanyEntity> companyGateway;

  private static final ILog log = new Log(ListCompaniesUseCase.class);

  private ListCompaniesUseCase(ICompanyGateway gateway) {
    this.companyGateway = gateway;
  }

  public static ListCompaniesUseCase of(ICompanyGateway companyGateway) {
    return new ListCompaniesUseCase(companyGateway);
  }

  public Pagination<CompanyOutput> execute(SearchQuery searchQuery) {
    log.info(FIND_ALL_COMPANIES);
    try {
      Pagination<CompanyOutput> outputPagination = this.companyGateway.findAll(searchQuery).map(CompanyOutput::of);
      log.info(FOUND_X_COMPANIES, outputPagination.items().size());
      return outputPagination;
    } catch (Exception e) {
      log.error(e.getMessage());
      return new Pagination<>(searchQuery.page(), searchQuery.perPage(), 0, new ArrayList<>());
    }
  }
}

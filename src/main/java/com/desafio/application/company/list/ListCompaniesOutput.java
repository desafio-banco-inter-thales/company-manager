package com.desafio.application.company.list;

import com.desafio.application.company.ICompanyEntity;
import com.desafio.application.company.create.CreateCompanyOutput;
import com.desafio.infrastructure.api.response.CompanyOutput;
import com.desafio.shared.notification.INotification;
import com.desafio.shared.notification.Notification;
import com.desafio.shared.notification.NotificationErrorProps;

import java.math.BigDecimal;
import java.time.Instant;

import static com.desafio.shared.utils.Constants.COMPANY_STR;

public record ListCompaniesOutput(String id, String name, String ticker, BigDecimal price, Boolean isActive, Instant createdAt, Instant updatedAt, Instant deletedAt, INotification notification) {

  public static ListCompaniesOutput of(ICompanyEntity company) {
    return new ListCompaniesOutput(
            company.id(),
            company.name(),
            company.ticker(),
            company.price(),
            company.isActive(),
            company.createdAt(),
            company.deletedAt(),
            company.updatedAt(),
            null
    );
  }

  public static ListCompaniesOutput of(INotification notification) {
    return new ListCompaniesOutput(null, null, null, null,null, null, null, null, notification);
  }

  public static ListCompaniesOutput of() {
    return new ListCompaniesOutput(null, null, null, null,null, null, null, null, null);
  }

  public static ListCompaniesOutput of(String message) {
    INotification notification = new Notification();
    notification.append(new NotificationErrorProps(message, COMPANY_STR));
    return new ListCompaniesOutput(null, null, null, null, null, null, null, null, notification);
  }

}

package com.desafio.application.company;

import com.desafio.shared.notification.INotification;

import java.math.BigDecimal;
import java.time.Instant;

public interface ICompanyEntity {

  String name();
  String id();
  String ticker();
  BigDecimal price();
  Boolean isActive();
  Instant createdAt();
  Instant updatedAt();
  Instant deletedAt();
  INotification notification();

  String toString();
}

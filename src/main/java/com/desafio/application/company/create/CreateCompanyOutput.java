package com.desafio.application.company.create;

import com.desafio.application.company.ICompanyEntity;
import com.desafio.shared.notification.INotification;
import com.desafio.shared.notification.Notification;
import com.desafio.shared.notification.NotificationErrorProps;

import java.math.BigDecimal;
import java.time.Instant;

public record CreateCompanyOutput(String id, String name, String ticker, BigDecimal price, Boolean isActive, Instant createdAt, Instant updatedAt, Instant deletedAt, INotification notification) implements ICompanyEntity {

  public static CreateCompanyOutput of() {
    return new CreateCompanyOutput(null, null, null, null, null, null, null, null, null);
  }

  public static CreateCompanyOutput of(INotification notification) {
    return new CreateCompanyOutput(null, null, null, null, null, null, null, null, notification);
  }

  public static CreateCompanyOutput of(ICompanyEntity company) {
    return new CreateCompanyOutput(
            company.id(),
            company.name(),
            company.ticker(),
            company.price(),
            company.isActive(),
            company.createdAt(),
            company.deletedAt(),
            company.updatedAt(),
            null
    );
  }

  public static CreateCompanyOutput of(String name, String ticker, BigDecimal price, Boolean isActive) {
    return new CreateCompanyOutput(null, name, ticker, price, isActive, null, null, null, null);
  }

  public static CreateCompanyOutput of(String id, String name, String ticker, BigDecimal price, Boolean isActive) {
    return new CreateCompanyOutput(id, name, ticker, price, isActive, null, null, null, null);
  }

  public static CreateCompanyOutput of(String message, String context) {
    INotification notification = new Notification();
    notification.append(new NotificationErrorProps(message, context));
    return new CreateCompanyOutput(null, null, null, null, null, null, null, null, notification);
  }
}

package com.desafio.application.company.create;

import com.desafio.application.company.CompanyDto;
import com.desafio.application.company.ICompanyEntity;
import com.desafio.application.company.ICompanyGateway;
import com.desafio.application.company.find.FindCompanyByTickerCommand;
import com.desafio.application.company.find.FindCompanyByTickerUseCase;
import com.desafio.domain.company.Company;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;

import java.math.BigDecimal;

import static com.desafio.shared.utils.ActionConstants.CREATE_COMPANY;
import static com.desafio.shared.utils.Constants.COMPANY_STR;
import static com.desafio.shared.utils.ErrorConstants.COMPANY_ALREADY_EXISTS;
import static com.desafio.shared.utils.ErrorConstants.GATEWAY_INSERT_STR;
import static com.desafio.shared.utils.SuccessConstants.*;

public class CreateCompanyUseCase {

  private static final ILog log = new Log(CreateCompanyUseCase.class);
  private final ICompanyGateway<ICompanyEntity, ICompanyEntity> companyGateway;
  private final FindCompanyByTickerUseCase findCompanyByTickerUseCase;

  private CreateCompanyUseCase(
          ICompanyGateway<ICompanyEntity, ICompanyEntity> gateway,
          FindCompanyByTickerUseCase findCompanyByTickerUseCase) {
    this.companyGateway = gateway;
    this.findCompanyByTickerUseCase = findCompanyByTickerUseCase;
  }

  public static CreateCompanyUseCase of(ICompanyGateway gateway, FindCompanyByTickerUseCase findCompanyByTickerUseCase) {
    return new CreateCompanyUseCase(gateway, findCompanyByTickerUseCase);
  }

  public CreateCompanyOutput execute(CreateCompanyCommand command) {
    CompanyDto company = this.createCompany(command);

    if (company.notification().hasErrors()) {
      log.info(company.notification().messages(""));
      return CreateCompanyOutput.of(company.notification());
    }

    log.info(COMPANY_VALIDATED, company.name());

    if (!this.hasCompany(company.ticker())) {
      String message = COMPANY_ALREADY_EXISTS.replace("{}", company.ticker());
      log.info(message);
      return CreateCompanyOutput.of(message, COMPANY_STR);
    }

    try {
      final var iCompanyEntity = this.companyGateway.save(company);
      log.info(COMPANY_INSERTED_ON_GATEWAY, command.ticker());
      return CreateCompanyOutput.of(iCompanyEntity);
    } catch (Exception e) {
      log.error(e.getMessage());
      return CreateCompanyOutput.of(GATEWAY_INSERT_STR.replace("{}", company.ticker()), COMPANY_STR);
    }
  }

  private boolean hasCompany(String ticker) {
    return findCompanyByTickerUseCase.execute(FindCompanyByTickerCommand.of(ticker)).notificationErrors() != null;
  }

  private CompanyDto createCompany(CreateCompanyCommand command) {
    final String name = command.name();
    final String ticker = command.ticker();
    final BigDecimal price = command.price();

    Company company = Company.of(name, ticker, price);

    log.info(CREATE_COMPANY, company.toString());

    company.validate();

    return CompanyDto.of(company);
  }
}

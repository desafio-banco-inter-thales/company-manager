package com.desafio.application.company.modify.update;

import com.desafio.application.company.modify.IModifyCompany;
import com.desafio.application.company.modify.ModifyCompanyCommand;
import com.desafio.application.company.modify.ModifyCompanyUseCase;
import com.desafio.domain.company.Company;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;

import java.math.BigDecimal;

import static com.desafio.shared.utils.ActionConstants.UPDATE_COMPANY;

public class UpdateCompanyUseCase implements IModifyCompany {

  private static final ILog log = new Log(UpdateCompanyUseCase.class);

  private UpdateCompanyUseCase() {
  }

  public static UpdateCompanyUseCase of() {
    return new UpdateCompanyUseCase();
  }

  @Override
  public Company execute(Company company, ModifyCompanyCommand command) {
    String ticker = command.ticker();
    BigDecimal price = command.price();
    log.info(UPDATE_COMPANY, ticker, price, company.getPrice());
    company.update(ticker, price).validate();
    return company;
  }

}

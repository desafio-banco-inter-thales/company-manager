package com.desafio.application.company.modify;

import com.desafio.application.company.CompanyDto;
import com.desafio.application.company.ICompanyEntity;
import com.desafio.application.company.ICompanyGateway;
import com.desafio.application.company.find.FindCompanyByTickerCommand;
import com.desafio.application.company.find.FindCompanyByTickerOutput;
import com.desafio.application.company.find.FindCompanyByTickerUseCase;
import com.desafio.domain.company.Company;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;

import static com.desafio.shared.utils.ErrorConstants.*;
import static com.desafio.shared.utils.SuccessConstants.COMPANY_UPDATED;

public class ModifyCompanyUseCase {

  private final ICompanyGateway<ICompanyEntity, ICompanyEntity> companyGateway;
  private final FindCompanyByTickerUseCase findCompanyByTickerUseCase;

  private static final ILog log = new Log(ModifyCompanyUseCase.class);

  private ModifyCompanyUseCase(
          ICompanyGateway<ICompanyEntity, ICompanyEntity> gateway,
          FindCompanyByTickerUseCase findCompanyByTickerUseCase) {
    this.companyGateway = gateway;
    this.findCompanyByTickerUseCase = findCompanyByTickerUseCase;
  }

  public static ModifyCompanyUseCase of(ICompanyGateway gateway, FindCompanyByTickerUseCase findCompanyByTickerUseCase) {
    return new ModifyCompanyUseCase(gateway, findCompanyByTickerUseCase);
  }

  public ModifyCompanyOutput execute(ModifyCompanyCommand command, IModifyCompany iModifyCompany) {
    if(iModifyCompany == null) {
      log.info(YOU_NEED_A_MODIFIER);
      return ModifyCompanyOutput.of(YOU_NEED_A_MODIFIER);
    }

    String ticker = command.ticker();
    final var findCompanyByTickerOutput = this.findCompany(ticker);

    if (findCompanyByTickerOutput.notificationErrors() != null) {
      log.info(COMPANY_NOT_UPDATED, command.ticker(), command.toString());
      return ModifyCompanyOutput.of(findCompanyByTickerOutput.notificationErrors());
    }

    Company company = this.createCompanyToUpdate(findCompanyByTickerOutput);
    company = iModifyCompany.execute(company, command);

    if (company.getNotification().hasErrors()) {
      log.info(COMPANY_NOT_UPDATED, command.ticker(), command.toString());
      return ModifyCompanyOutput.of(company.getNotification());
    }

    return this.updateCompany(CompanyDto.of(company));
  }

  private ModifyCompanyOutput updateCompany(ICompanyEntity company) {
    try {
      this.companyGateway.save(company);
      log.info(COMPANY_UPDATED);
    } catch (Exception e) {
      String message = GATEWAY_UPDATE_STR.replace("{}", company.ticker());
      log.error(e.getMessage());
      return ModifyCompanyOutput.of(message);
    }

    return ModifyCompanyOutput.of(company);
  }

  private Company createCompanyToUpdate(FindCompanyByTickerOutput findCompanyByTickerOutput) {
    final var companyDto = findCompanyByTickerOutput.company();
    return Company.of(
            companyDto.id(),
            companyDto.name(),
            companyDto.ticker(),
            companyDto.price(),
            companyDto.isActive()
    );
  }

  private FindCompanyByTickerOutput findCompany(String ticker) {
    return findCompanyByTickerUseCase.execute(FindCompanyByTickerCommand.of(ticker));
  }
}

package com.desafio.application.company.modify.deactivate;

import com.desafio.application.company.modify.IModifyCompany;
import com.desafio.application.company.modify.ModifyCompanyCommand;
import com.desafio.domain.company.Company;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;

import static com.desafio.shared.utils.ActionConstants.DEACTIVATE_COMPANY;

public class DeactivateCompanyUseCase implements IModifyCompany {

  private final ILog log = new Log(DeactivateCompanyUseCase.class);

  private DeactivateCompanyUseCase() {
  }

  public static DeactivateCompanyUseCase of() {
    return new DeactivateCompanyUseCase();
  }

  @Override
  public Company execute(Company company, ModifyCompanyCommand command) {
    log.info(DEACTIVATE_COMPANY, command.ticker());
    company.deactivate().validate();
    return company;
  }

}

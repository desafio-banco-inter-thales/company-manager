package com.desafio.application.company.modify;

import com.desafio.application.company.ICompanyEntity;
import com.desafio.application.company.create.CreateCompanyOutput;
import com.desafio.shared.notification.INotification;
import com.desafio.shared.notification.Notification;

import java.math.BigDecimal;
import java.time.Instant;

import static com.desafio.shared.utils.Constants.COMPANY_STR;

public record ModifyCompanyOutput(String id, String name, String ticker, BigDecimal price, Boolean isActive, Instant createdAt, Instant updatedAt, Instant deletedAt, INotification notification) implements ICompanyEntity {

  public static ModifyCompanyOutput of(INotification notification) {
    return new ModifyCompanyOutput(null, null, null, null, null, null, null, null, notification);
  }

  public static ModifyCompanyOutput of(String message) {
    INotification notification = new Notification();
    notification.append(message, COMPANY_STR);
    return new ModifyCompanyOutput(null, null, null, null, null, null, null, null, notification);
  }

  public static ModifyCompanyOutput of(ICompanyEntity company) {
    return new ModifyCompanyOutput(
            company.id(),
            company.name(),
            company.ticker(),
            company.price(),
            company.isActive(),
            company.createdAt(),
            company.deletedAt(),
            company.updatedAt(),
            null
    );
  }
}

package com.desafio.application.company.modify;

import com.desafio.domain.company.Company;

public interface IModifyCompany {

  Company execute(Company company, ModifyCompanyCommand command);

}

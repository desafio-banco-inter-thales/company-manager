package com.desafio.application.company.modify.activate;

import com.desafio.application.company.modify.IModifyCompany;
import com.desafio.application.company.modify.ModifyCompanyCommand;
import com.desafio.domain.company.Company;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;

import static com.desafio.shared.utils.ActionConstants.ACTIVATE_COMPANY;

public class ActivateCompanyUseCase implements IModifyCompany {

  private final ILog log = new Log(ActivateCompanyUseCase.class);

  private ActivateCompanyUseCase() {
  }

  public static ActivateCompanyUseCase of() {
    return new ActivateCompanyUseCase();
  }

  @Override
  public Company execute(Company company, ModifyCompanyCommand command) {
    log.info(ACTIVATE_COMPANY, command.ticker());
    company.activate().validate();
    return company;
  }
}

package com.desafio.application.company.modify;

import java.math.BigDecimal;

public record ModifyCompanyCommand(String ticker, BigDecimal price) {

  public static ModifyCompanyCommand of(String ticker, BigDecimal price) {
    return new ModifyCompanyCommand(ticker, price);
  }

  public static ModifyCompanyCommand of(String ticker) {
    return new ModifyCompanyCommand(ticker, null);
  }

  @Override
  public String toString() {
    return "ModifyCompanyCommand{" +
            "ticker='" + ticker + '\'' +
            ", price=" + price +
            '}';
  }
}

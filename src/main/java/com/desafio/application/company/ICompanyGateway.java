package com.desafio.application.company;

import com.desafio.infrastructure.api.pagination.Pagination;
import com.desafio.infrastructure.api.pagination.SearchQuery;

import java.util.List;
import java.util.Optional;

public interface ICompanyGateway<I extends ICompanyEntity, O extends ICompanyEntity> {

  Pagination<O> findAll(SearchQuery searchQuery);
  Optional<O> findByTicker(String ticker);

  O save(I company);
  void delete(I company);

  List<O> findCompaniesUserDoesNotHave(List<String> tickers);

}

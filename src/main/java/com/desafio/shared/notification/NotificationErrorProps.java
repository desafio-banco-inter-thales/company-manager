package com.desafio.shared.notification;

public record NotificationErrorProps(String message, String context) { }

package com.desafio.shared.utils;

public class SuccessConstants {
  public static final String COMPANY_FOUND = "COMPANY FOUND: '{}'.";
  public static final String USER_FOUND = "USER FOUND: '{}'.";
  public static final String ADM_USER_FOUND = "ADM USER FOUND: '{}'.";
  public static final String COMPANY_UPDATED = "COMPANY SUCCESSFULLY MODIFIED.";
  public static final String USER_VALIDATED = "OBJECT USER '{}' SUCCESSFULLY VALIDATED.";
  public static final String COMPANY_VALIDATED = "OBJECT COMPANY '{}' SUCCESSFULLY VALIDATED.";
  public static final String USER_INSERTED_ON_GATEWAY = "USER WITH CPF: '{}' SUCCESSFULLY INSERTED ON GATEWAY.";
  public static final String COMPANY_INSERTED_ON_GATEWAY = "COMPANY WITH TICKER: '{}' SUCCESSFULLY INSERTED ON GATEWAY.";
  public static final String COMPANY_REMOVED_ON_GATEWAY = "COMPANY WITH TICKER: '{}' SUCCESSFULLY DELETED FROM GATEWAY.";
  public static final String FOUND_X_COMPANIES = "GOT {} COMPANIES.";
  public static final String EVENT_SENT = "EVENT SENT WITH PAYLOAD: {}";
}

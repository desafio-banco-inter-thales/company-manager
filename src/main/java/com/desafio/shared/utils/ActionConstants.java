package com.desafio.shared.utils;

public class ActionConstants {
  public static final String CREATE_USER = "CREATING USER: '{}'...";
  public static final String CREATE_COMPANY = "CREATING COMPANY: '{}'...";
  public static final String DELETE_COMPANY = "DELETING COMPANY WITH TICKER: '{}'...";
  public static final String FIND_COMPANY = "GETTING COMPANY WITH TICKER: '{}'...";
  public static final String DEACTIVATE_COMPANY = "DEACTIVATING COMPANY WITH TICKER: '{}'...";
  public static final String ACTIVATE_COMPANY = "ACTIVATING COMPANY WITH TICKER: '{}'...";
  public static final String FIND_ALL_COMPANIES = "GETTING ALL COMPANIES...";
  public static final String UPDATE_COMPANY = "UPDATING STATE OF COMPANY WITH TICKER: '{}' AND A NEW PRICE: '{}', SUBSTITUTING OLD ONE: '{}'";
  public static final String FIND_BY_CPF = "GETTING USER BY CPF {}...";
  public static final String FIND_ADM_BY_CPF = "GETTING ADM USER BY CPF {}...";
}

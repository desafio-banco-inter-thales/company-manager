package com.desafio.infrastructure.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.desafio"})
public class WebServerConfig {
  public WebServerConfig() {
  }
}

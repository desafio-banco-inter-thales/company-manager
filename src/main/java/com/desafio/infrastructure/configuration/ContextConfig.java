package com.desafio.infrastructure.configuration;

import com.desafio.application.company.create.CreateCompanyUseCase;
import com.desafio.application.company.delete.DeleteCompanyUseCase;
import com.desafio.application.company.find.FindCompanyByTickerUseCase;
import com.desafio.application.company.list.ListCompaniesUseCase;
import com.desafio.application.company.modify.ModifyCompanyUseCase;
import com.desafio.application.company.modify.activate.ActivateCompanyUseCase;
import com.desafio.application.company.modify.deactivate.DeactivateCompanyUseCase;
import com.desafio.application.company.modify.update.UpdateCompanyUseCase;
import com.desafio.application.user.adm.FindAdmUserByCpfUseCase;
import com.desafio.application.user.create.CreateUserUseCase;
import com.desafio.application.user.find.FindUserByCpfUseCase;
import com.desafio.infrastructure.api.service.CreateCompanyService;
import com.desafio.infrastructure.gateway.company.CompanySqlGateway;
import com.desafio.infrastructure.gateway.user.UserSqlGateway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ContextConfig {

  private final CompanySqlGateway companyGateway;

  private final UserSqlGateway userGateway;

  public ContextConfig(
          CompanySqlGateway companyGateway,
          UserSqlGateway userGateway) {
    this.companyGateway = companyGateway;
    this.userGateway = userGateway;
  }

  @Bean
  public FindCompanyByTickerUseCase findCompanyByTickerUseCase() {
    return FindCompanyByTickerUseCase.of(this.companyGateway);
  }

  @Bean
  public CreateCompanyUseCase createCompanyUseCase() {
    return CreateCompanyUseCase.of(this.companyGateway, findCompanyByTickerUseCase());
  }

  @Bean
  public ListCompaniesUseCase listCompaniesUseCase() {
    return ListCompaniesUseCase.of(this.companyGateway);
  }

  @Bean
  public DeleteCompanyUseCase deleteCompanyUseCase() {
    return DeleteCompanyUseCase.of(this.companyGateway, findCompanyByTickerUseCase());
  }

  @Bean
  public ActivateCompanyUseCase activateCompanyUseCase() {return ActivateCompanyUseCase.of();}

  @Bean
  public DeactivateCompanyUseCase deactivateCompanyUseCase() {return DeactivateCompanyUseCase.of();}

  @Bean
  public UpdateCompanyUseCase updateCompanyUseCase() {
    return UpdateCompanyUseCase.of();
  }

  @Bean
  public ModifyCompanyUseCase modifyCompanyUseCase() {
    return ModifyCompanyUseCase.of(this.companyGateway, findCompanyByTickerUseCase());
  }

  @Bean
  public FindUserByCpfUseCase findUserByCpfUseCase() {
    return FindUserByCpfUseCase.of(this.userGateway);
  }

  @Bean
  public FindAdmUserByCpfUseCase findAdmUserByCpfUseCase() {
    return FindAdmUserByCpfUseCase.of(this.userGateway);
  }

  @Bean
  public CreateUserUseCase createUserUseCase() {
    return CreateUserUseCase.of(this.userGateway, findUserByCpfUseCase());
  }

//  @Bean
//  public CreateCompanyService createCompanyService() {
//    return CreateCompanyService.of(createCompanyUseCase(), findUserByCpfUseCase());
//  }

}

package com.desafio.infrastructure.stream.event;

import java.math.BigDecimal;

public record CompanyEvent(String type, String id, String name, String ticker, BigDecimal price, boolean isActive, String userCpf) {

  public static CompanyEvent of(String type, String id, String name, String ticker, BigDecimal price, boolean isActive, String userCpf) {
    return new CompanyEvent(type, id, name, ticker, price, isActive, userCpf);
  }

  public static CompanyEvent of(String type, String name, String ticker, BigDecimal price, boolean isActive, String userCpf) {
    return new CompanyEvent(type, null, name, ticker, price, isActive, userCpf);
  }

  public String getBasicEventData() {
    return String.format(
            "[type=%s, name=%s, ticker=%s, price=%s, isActive=%s]",
            type, name, ticker, price, isActive);
  }

}
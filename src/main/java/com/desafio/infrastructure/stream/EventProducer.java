package com.desafio.infrastructure.stream;

import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Component;

import static com.desafio.shared.utils.SuccessConstants.EVENT_SENT;

@Component
public class EventProducer {

  private static final ILog log = new Log(EventProducer.class);

  private final StreamBridge streamBridge;

  public EventProducer(StreamBridge streamBridge) {
    this.streamBridge = streamBridge;
  }

  public void produce(String event) {
    streamBridge.send("companyEvent-out-0", event);
    log.info(EVENT_SENT, event);
  }
}

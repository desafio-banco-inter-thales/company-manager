package com.desafio.infrastructure.stream.service;

import com.desafio.application.user.IUserGateway;
import com.desafio.application.user.create.CreateUserCommand;
import com.desafio.application.user.create.CreateUserUseCase;
import com.desafio.infrastructure.stream.event.UserEvent;
import org.springframework.stereotype.Service;

@Service
public class UserService {

  private final CreateUserUseCase useCase;

  private UserService(CreateUserUseCase useCase) {
    this.useCase = useCase;
  }

  public static UserService of(CreateUserUseCase useCase) {
    return new UserService(useCase);
  }

  public void execute(UserEvent event) {
    final var command = CreateUserCommand.of(event.name(), event.cpf(), event.isAdm());
    this.useCase.execute(command);
  }
}

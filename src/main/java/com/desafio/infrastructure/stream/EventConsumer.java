package com.desafio.infrastructure.stream;

import com.desafio.infrastructure.stream.event.CompanyEvent;
import com.desafio.infrastructure.stream.event.UserEvent;
import com.desafio.infrastructure.stream.service.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.Consumer;

@Configuration
public class EventConsumer {

  private final UserService userService;

  public EventConsumer(UserService userService) {
    this.userService = userService;
  }

  @Bean
  public Consumer<UserEvent> userCreated() {
    return event -> userService.execute(event);
  }

}

package com.desafio.infrastructure.gateway.company;

import com.desafio.application.company.CompanyDto;
import com.desafio.application.company.ICompanyEntity;
import com.desafio.shared.notification.INotification;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.Instant;

@Entity
@Table(
        name = "company"
)
public class CompanyJpaEntity implements ICompanyEntity {

  @Id
  private String id;

  @Column(
          name = "name",
          length = 30,
          nullable = false
  )
  private String name;

  @Column(
          name = "ticker",
          nullable = false
  )
  private String ticker;

  @Column(
          name = "price",
          nullable = false
  )
  private BigDecimal price;

  @Column(
          name = "is_active",
          nullable = false
  )
  private boolean isActive;

  @Column(
          name = "created_at",
          columnDefinition = "DATETIME(6)",
          nullable = false
  )
  private Instant created_at;

  @Column(
          name = "updated_at",
          columnDefinition = "DATETIME(6)",
          nullable = false
  )
  private Instant updated_at;

  @Column(
          name = "deleted_at",
          columnDefinition = "DATETIME(6)",
          nullable = true
  )
  private Instant deleted_at;

  public CompanyJpaEntity() {
  }

  private CompanyJpaEntity(String id, String name, String ticker, BigDecimal price, Boolean isActive, Instant created_at, Instant updated_at, Instant deleted_at) {
    this.id = id;
    this.name = name;
    this.ticker = ticker;
    this.price = price;
    this.isActive = isActive;
    this.created_at = created_at;
    this.updated_at = updated_at;
    this.deleted_at = deleted_at;
  }

  public static CompanyJpaEntity of(ICompanyEntity iCompanyEntity) {
    return new CompanyJpaEntity(
            iCompanyEntity.id(),
            iCompanyEntity.name(),
            iCompanyEntity.ticker(),
            iCompanyEntity.price(),
            iCompanyEntity.isActive(),
            iCompanyEntity.createdAt(),
            iCompanyEntity.updatedAt(),
            iCompanyEntity.deletedAt());
  }

  public CompanyDto toAggregate() {
    return CompanyDto.of(this);
  }

  @Override
  public String name() {
    return this.name;
  }

  @Override
  public String id() {
    return this.id;
  }

  @Override
  public String ticker() {
    return this.ticker;
  }

  @Override
  public BigDecimal price() {
    return this.price;
  }

  @Override
  public Boolean isActive() {
    return this.isActive;
  }

  @Override
  public Instant createdAt() {
    return this.created_at;
  }

  @Override
  public Instant updatedAt() {
    return this.updated_at;
  }

  @Override
  public Instant deletedAt() {
    return this.deleted_at;
  }

  @Override
  public INotification notification() {
    return null;
  }

  @Override
  public String toString() {
    return "CompanyJpaEntity{" +
            "id='" + id + '\'' +
            ", name='" + name + '\'' +
            ", ticker='" + ticker + '\'' +
            ", price=" + price +
            ", isActive=" + isActive +
            ", created_at=" + created_at +
            ", updated_at=" + updated_at +
            ", deleted_at=" + deleted_at +
            '}';
  }
}

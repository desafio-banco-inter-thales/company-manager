package com.desafio.infrastructure.gateway.company;

import com.desafio.application.company.CompanyDto;
import com.desafio.application.company.ICompanyGateway;
import com.desafio.infrastructure.api.pagination.Pagination;
import com.desafio.infrastructure.api.pagination.SearchQuery;
import org.hibernate.criterion.SimpleExpression;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hibernate.criterion.Restrictions.eq;

@Component
public class CompanySqlGateway implements ICompanyGateway<CompanyDto, CompanyJpaEntity> {
  private final CompanyRepository companyRepository;

  public CompanySqlGateway(CompanyRepository companyRepository) {
    this.companyRepository = companyRepository;
  }

  @Override
  public CompanyJpaEntity save(CompanyDto company) {
    return this.companyRepository.save(CompanyJpaEntity.of(company));
  }

  @Override
  public Pagination<CompanyJpaEntity> findAll(SearchQuery searchQuery) {
    final var page = PageRequest.of(
            searchQuery.page(),
            searchQuery.perPage()
    );

    List<CompanyJpaEntity> list = Collections.emptyList();
    Page<CompanyJpaEntity> pageResult = new PageImpl<>(list);

    if(searchQuery.isActive() == null)
      pageResult = this.companyRepository.findAll(page);
    else
      pageResult = this.companyRepository.findByIsActive(searchQuery.isActive(), page);

    return new Pagination<>(
            pageResult.getNumber(),
            pageResult.getSize(),
            pageResult.getTotalElements(),
            pageResult.toList()
    );
  }

  @Override
  public List<CompanyJpaEntity> findCompaniesUserDoesNotHave(List<String> tickers) {
    return null;
  }


  public Optional<CompanyJpaEntity> findByTicker(String ticker) {
    final var optionalCompanyJpaEntity = this.companyRepository.findByTicker(ticker);

    if(optionalCompanyJpaEntity.isEmpty())
      return Optional.empty();

    return Optional.of(optionalCompanyJpaEntity.get());
  }

  @Override
  public void delete(CompanyDto company) {
    this.companyRepository.delete(CompanyJpaEntity.of(company));
  }

  private SimpleExpression assembleSpecification(final boolean isActive) {
    return eq("is_active", isActive);
  }
}
package com.desafio.infrastructure.gateway.company;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CompanyRepository extends JpaRepository<CompanyJpaEntity, String> {
  Optional<CompanyJpaEntity> findByTicker(String ticker);

  Page<CompanyJpaEntity> findAll(Specification<CompanyJpaEntity> whereClause, Pageable page);

  Page<CompanyJpaEntity> findByIsActive(boolean isActive, Pageable pageable);

}
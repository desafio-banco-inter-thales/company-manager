package com.desafio.infrastructure.gateway.user;

import com.desafio.application.user.IUserEntity;
import com.desafio.application.user.IUserGateway;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserSqlGateway implements IUserGateway {

  private final UserRepository userRepository;

  public UserSqlGateway(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  public Optional<IUserEntity> findByCpf(String cpf) {
    return this.userRepository.findByCpf(cpf);
  }

  @Override
  public Optional<IUserEntity> findAdmByCpf(String cpf) {
    return this.userRepository.findByCpf(cpf);
  }

  @Override
  public IUserEntity save(IUserEntity user) {
    return this.userRepository.save(UserJpaEntity.of(user));
  }
}

package com.desafio.infrastructure.gateway.user;

import com.desafio.application.user.IUserEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class UserJpaEntity implements IUserEntity {

  @Id
  private String id;

  @Column(name = "cpf", length = 11, nullable = false)
  private String cpf;

  @Column(name = "name", length = 11, nullable = false)
  private String name;

  @Column(name = "is_adm", length = 11, nullable = false)
  private Boolean isAdm;

  private UserJpaEntity() {
  }

  private UserJpaEntity(String id, String cpf, String name, Boolean isAdm) {
    this.id = id;
    this.cpf = cpf;
    this.name = name;
    this.isAdm = isAdm;
  }

  public static UserJpaEntity of(IUserEntity iUserEntity) {
    return new UserJpaEntity(iUserEntity.id(), iUserEntity.cpf(), iUserEntity.name(), iUserEntity.isAdm());
  }

  public static UserJpaEntity of(String id) {
    return new UserJpaEntity(id, null, null, null);
  }

  @Override
  public String id() {
    return id;
  }

  @Override
  public Boolean isAdm() {
    return this.isAdm;
  }

  @Override
  public String name() {
    return this.name;
  }

  @Override
  public String cpf() {
    return cpf;
  }

  @Override
  public String toString() {
    return "UserJPAEntity{" +
            "id='" + id + '\'' +
            ", cpf='" + cpf + '\'' +
            ", name='" + name + '\'' +
            ", idAdm='" + isAdm + '\'' +
            '}';
  }
}

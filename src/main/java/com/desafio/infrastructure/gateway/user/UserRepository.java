package com.desafio.infrastructure.gateway.user;

import com.desafio.application.user.IUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserJpaEntity, String> {

  Optional<IUserEntity> findByCpf(String cpf);

  @Query("FROM UserJpaEntity WHERE cpf = ?1 and isAdm = true")
  Optional<IUserEntity> findAdmByCpf(String cpf);

}

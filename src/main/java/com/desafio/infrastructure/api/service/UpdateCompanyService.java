package com.desafio.infrastructure.api.service;

import com.desafio.application.company.modify.ModifyCompanyCommand;
import com.desafio.application.company.modify.ModifyCompanyUseCase;
import com.desafio.application.company.modify.update.UpdateCompanyUseCase;
import com.desafio.application.user.adm.FindAdmUserByCpfCommand;
import com.desafio.application.user.adm.FindAdmUserByCpfUseCase;
import com.desafio.application.user.find.FindUserByCpfCommand;
import com.desafio.application.user.find.FindUserByCpfUseCase;
import com.desafio.infrastructure.api.request.UpdateCompanyRequest;
import com.desafio.infrastructure.api.response.CompanyOutput;
import com.desafio.infrastructure.stream.event.CompanyEvent;
import com.desafio.infrastructure.stream.EventProducer;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import static com.desafio.shared.utils.ErrorConstants.PARSE_ERROR;

@Service
public class UpdateCompanyService {
  private final ModifyCompanyUseCase modifyCompanyUseCase;
  private final FindAdmUserByCpfUseCase findAdmUserByCpfUseCase;
  private final EventProducer eventProducer;
  private static final ObjectMapper mapper = new ObjectMapper();
  private static final ILog log = new Log(UpdateCompanyService.class);

  public UpdateCompanyService(
          ModifyCompanyUseCase modifyCompanyUseCase,
          FindAdmUserByCpfUseCase findAdmUserByCpfUseCase,
          EventProducer eventProducer) {
    this.modifyCompanyUseCase = modifyCompanyUseCase;
    this.findAdmUserByCpfUseCase = findAdmUserByCpfUseCase;
    this.eventProducer = eventProducer;
  }

  public CompanyOutput execute(String ticker, UpdateCompanyRequest input) {
    String userCpf = input.userCpf();
    BigDecimal companyPrice = input.price();

    final var validatedUser = this.findAdmUserByCpfUseCase.execute(FindAdmUserByCpfCommand.of(userCpf));
    if (validatedUser.notificationErrors() != null)
      return CompanyOutput.of(validatedUser.notificationErrors());

    final var command = ModifyCompanyCommand.of(ticker, companyPrice);
    final var response = this.modifyCompanyUseCase.execute(command, UpdateCompanyUseCase.of());

    if (response.notification() != null)
      return CompanyOutput.of(response.notification());

    CompanyEvent event = CompanyEvent.of("save", response.name(), response.ticker(), response.price(), response.isActive(), userCpf);

    try {
      this.eventProducer.produce(mapper.writeValueAsString(event));
    }catch (Exception e) {
      log.error(e.getMessage());
      return CompanyOutput.of(PARSE_ERROR + " " + e.getMessage());
    }

    return CompanyOutput.of(response);
  }
}

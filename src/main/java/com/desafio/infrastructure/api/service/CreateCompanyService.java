package com.desafio.infrastructure.api.service;

import com.desafio.application.company.create.CreateCompanyCommand;
import com.desafio.application.company.create.CreateCompanyUseCase;
import com.desafio.application.user.adm.FindAdmUserByCpfCommand;
import com.desafio.application.user.adm.FindAdmUserByCpfUseCase;
import com.desafio.application.user.find.FindUserByCpfCommand;
import com.desafio.application.user.find.FindUserByCpfUseCase;
import com.desafio.infrastructure.api.request.CreateCompanyRequest;
import com.desafio.infrastructure.api.response.CompanyOutput;
import com.desafio.infrastructure.stream.event.CompanyEvent;
import com.desafio.infrastructure.stream.EventProducer;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import static com.desafio.shared.utils.ErrorConstants.PARSE_ERROR;

@Service
public class CreateCompanyService {

  private final CreateCompanyUseCase createCompanyUseCase;
  private final FindAdmUserByCpfUseCase findAdmUserByCpfUseCase;
  private final EventProducer eventProducer;
  private static final ObjectMapper mapper = new ObjectMapper();
  private static final ILog log = new Log(CreateCompanyService.class);

  private CreateCompanyService(
          CreateCompanyUseCase createCompanyUseCase,
          FindAdmUserByCpfUseCase findAdmUserByCpfUseCase,
          EventProducer eventProducer) {
    this.createCompanyUseCase = createCompanyUseCase;
    this.findAdmUserByCpfUseCase = findAdmUserByCpfUseCase;
    this.eventProducer = eventProducer;
  }

  public static CreateCompanyService of(
          CreateCompanyUseCase createCompanyUseCase,
          FindAdmUserByCpfUseCase findUserByCpfUseCase,
          EventProducer eventProducer) {
    return new CreateCompanyService(createCompanyUseCase, findUserByCpfUseCase, eventProducer);
  }

  public CompanyOutput execute(CreateCompanyRequest request) {
    String userCpf = request.userCpf();
    String companyName = request.name();
    String companyTicker = request.ticker();
    BigDecimal companyPrice = request.price();
    Boolean isCompanyActive = request.isActive();

    final var validatedUser = this.findAdmUserByCpfUseCase.execute(FindAdmUserByCpfCommand.of(userCpf));
    if (validatedUser.notificationErrors() != null)
      return CompanyOutput.of(validatedUser.notificationErrors());

    final var command = CreateCompanyCommand.of(companyName, companyTicker, companyPrice, isCompanyActive);
    final var response = this.createCompanyUseCase.execute(command);

    if (response.notification() != null)
      return CompanyOutput.of(response.notification());

    CompanyEvent event = CompanyEvent.of("save", response.id(), response.name(), response.ticker(), response.price(), response.isActive(), userCpf);

    try {
      this.eventProducer.produce(mapper.writeValueAsString(event));
    }catch (Exception e) {
      log.error(e.getMessage());
      return CompanyOutput.of(PARSE_ERROR + " " + e.getMessage());
    }

    return CompanyOutput.of(response);
  }
}

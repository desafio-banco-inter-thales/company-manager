package com.desafio.infrastructure.api.service;

import com.desafio.application.company.create.CreateCompanyUseCase;
import com.desafio.application.company.modify.ModifyCompanyCommand;
import com.desafio.application.company.modify.ModifyCompanyUseCase;
import com.desafio.application.company.modify.activate.ActivateCompanyUseCase;
import com.desafio.application.user.adm.FindAdmUserByCpfCommand;
import com.desafio.application.user.adm.FindAdmUserByCpfUseCase;
import com.desafio.application.user.find.FindUserByCpfCommand;
import com.desafio.application.user.find.FindUserByCpfUseCase;
import com.desafio.infrastructure.api.request.ActivationCompanyRequest;
import com.desafio.infrastructure.api.response.CompanyOutput;
import com.desafio.infrastructure.stream.event.CompanyEvent;
import com.desafio.infrastructure.stream.EventProducer;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import static com.desafio.shared.utils.ErrorConstants.PARSE_ERROR;
import static com.desafio.shared.utils.ErrorConstants.USER_NOT_ADM;

@Service
public class ActivateCompanyService {

  private final ModifyCompanyUseCase modifyCompanyUseCase;
  private final FindAdmUserByCpfUseCase findAdmUserByCpfUseCase;
  private final EventProducer eventProducer;
  private static final ObjectMapper mapper = new ObjectMapper();
  private static final ILog log = new Log(UpdateCompanyService.class);

  private ActivateCompanyService(
          ModifyCompanyUseCase modifyCompanyUseCase,
          FindAdmUserByCpfUseCase findAdmUserByCpfUseCase,
          EventProducer eventProducer) {
    this.modifyCompanyUseCase = modifyCompanyUseCase;
    this.findAdmUserByCpfUseCase = findAdmUserByCpfUseCase;
    this.eventProducer = eventProducer;
  }

  public static ActivateCompanyService of(
          ModifyCompanyUseCase modifyCompanyUseCase,
          FindAdmUserByCpfUseCase findUserByCpfUseCase,
          EventProducer eventProducer) {
    return new ActivateCompanyService(modifyCompanyUseCase, findUserByCpfUseCase, eventProducer);
  }

  public CompanyOutput execute(String ticker, ActivationCompanyRequest request) {
    String userCpf = request.userCpf();

    final var validatedUser = this.findAdmUserByCpfUseCase.execute(FindAdmUserByCpfCommand.of(userCpf));
    if (validatedUser.notificationErrors() != null)
      return CompanyOutput.of(validatedUser.notificationErrors());

    final var response = this.modifyCompanyUseCase.execute(ModifyCompanyCommand.of(ticker), ActivateCompanyUseCase.of());

    if (response.notification() != null)
      return CompanyOutput.of(response.notification());

    CompanyEvent event = CompanyEvent.of("save", response.name(), response.ticker(), response.price(), response.isActive(), userCpf);

    try {
      this.eventProducer.produce(mapper.writeValueAsString(event));
    }catch (Exception e) {
      return CompanyOutput.of(PARSE_ERROR + " " + e.getMessage());
    }

    return CompanyOutput.of(response);
  }

}

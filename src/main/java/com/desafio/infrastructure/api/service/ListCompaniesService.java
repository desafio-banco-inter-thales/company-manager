package com.desafio.infrastructure.api.service;

import com.desafio.application.company.CompanyDto;
import com.desafio.application.company.list.ListCompaniesUseCase;
import com.desafio.application.user.find.FindUserByCpfCommand;
import com.desafio.application.user.find.FindUserByCpfUseCase;
import com.desafio.infrastructure.api.pagination.Pagination;
import com.desafio.infrastructure.api.pagination.SearchQuery;
import com.desafio.infrastructure.api.response.CompanyOutput;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ListCompaniesService {

  private final ListCompaniesUseCase listCompaniesUseCase;
  private final FindUserByCpfUseCase findUserByCpfUseCase;

  public ListCompaniesService(ListCompaniesUseCase listCompaniesUseCase, FindUserByCpfUseCase findUserByCpfUseCase) {
    this.listCompaniesUseCase = listCompaniesUseCase;
    this.findUserByCpfUseCase = findUserByCpfUseCase;
  }

  public Pagination<CompanyOutput> execute(SearchQuery searchQuery) {
    final var validatedUser = this.findUserByCpfUseCase.execute(FindUserByCpfCommand.of(searchQuery.userCpf()));
    if (validatedUser.notificationErrors() != null) {
      return new Pagination<>(
              searchQuery.page(),
              searchQuery.perPage(),
              0,
              List.<CompanyOutput>of(CompanyOutput.of(validatedUser.notificationErrors()))
      );
    }

    return listCompaniesUseCase.execute(searchQuery);
  }
}

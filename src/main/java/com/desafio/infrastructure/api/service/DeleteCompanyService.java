package com.desafio.infrastructure.api.service;

import com.desafio.application.company.delete.DeleteCompanyCommand;
import com.desafio.application.company.delete.DeleteCompanyUseCase;
import com.desafio.application.user.adm.FindAdmUserByCpfCommand;
import com.desafio.application.user.adm.FindAdmUserByCpfUseCase;
import com.desafio.application.user.find.FindUserByCpfCommand;
import com.desafio.application.user.find.FindUserByCpfUseCase;
import com.desafio.infrastructure.api.response.CompanyOutput;
import com.desafio.infrastructure.stream.event.CompanyEvent;
import com.desafio.infrastructure.stream.EventProducer;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import static com.desafio.shared.utils.ErrorConstants.PARSE_ERROR;

@Service
public class DeleteCompanyService {
  private final DeleteCompanyUseCase deleteCompanyUseCase;
  private final FindAdmUserByCpfUseCase findAdmUserByCpfUseCase;

  private final EventProducer eventProducer;

  private static final ObjectMapper mapper = new ObjectMapper();

  private static final ILog log = new Log(DeleteCompanyService.class);

  public DeleteCompanyService(
          DeleteCompanyUseCase deleteCompanyUseCase,
          FindAdmUserByCpfUseCase findAdmUserByCpfUseCase,
          EventProducer eventProducer) {
    this.deleteCompanyUseCase = deleteCompanyUseCase;
    this.findAdmUserByCpfUseCase = findAdmUserByCpfUseCase;
    this.eventProducer = eventProducer;
  }


  public CompanyOutput execute(String ticker, String userCpf) {
    final var validatedUser = this.findAdmUserByCpfUseCase.execute(FindAdmUserByCpfCommand.of(userCpf));
    if (validatedUser.notificationErrors() != null)
      return CompanyOutput.of(validatedUser.notificationErrors());

    final var command = DeleteCompanyCommand.of(ticker);
    final var output = this.deleteCompanyUseCase.execute(command);

    if (output.notification() != null)
      return CompanyOutput.of(output.notification());

    final var response = output.company();
    CompanyEvent event = CompanyEvent.of("delete", response.id(), response.name(), response.ticker(), response.price(), response.isActive(), userCpf);

    try {
      this.eventProducer.produce(mapper.writeValueAsString(event));
    } catch (Exception e) {
      return CompanyOutput.of(PARSE_ERROR + " " + e.getMessage());
    }

    return CompanyOutput.of();
  }
}

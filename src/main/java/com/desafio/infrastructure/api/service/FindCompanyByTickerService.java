package com.desafio.infrastructure.api.service;

import com.desafio.application.company.find.FindCompanyByTickerCommand;
import com.desafio.application.company.find.FindCompanyByTickerOutput;
import com.desafio.application.company.find.FindCompanyByTickerUseCase;
import com.desafio.application.user.adm.FindAdmUserByCpfCommand;
import com.desafio.application.user.find.FindUserByCpfCommand;
import com.desafio.application.user.find.FindUserByCpfUseCase;
import com.desafio.infrastructure.api.request.FindCompanyByTickerRequest;
import com.desafio.infrastructure.api.response.CompanyOutput;
import org.springframework.stereotype.Service;

@Service
public class FindCompanyByTickerService {

  private final FindCompanyByTickerUseCase findCompanyByTickerUseCase;

  private final FindUserByCpfUseCase findUserByCpfUseCase;

  public FindCompanyByTickerService(
          FindCompanyByTickerUseCase findCompanyByTickerUseCase,
          FindUserByCpfUseCase findUserByCpfUseCase) {
    this.findCompanyByTickerUseCase = findCompanyByTickerUseCase;
    this.findUserByCpfUseCase = findUserByCpfUseCase;
  }

  public CompanyOutput execute(String ticker, String userCpf) {
    final var validatedUser = this.findUserByCpfUseCase.execute(FindUserByCpfCommand.of(userCpf));
    if (validatedUser.notificationErrors() != null)
      return CompanyOutput.of(validatedUser.notificationErrors());

    FindCompanyByTickerOutput output = this.findCompanyByTickerUseCase.execute(FindCompanyByTickerCommand.of(ticker));

    if(output.company() != null)
      return CompanyOutput.of(output.company());
    else
      return CompanyOutput.of(output.notificationErrors());
  }
}

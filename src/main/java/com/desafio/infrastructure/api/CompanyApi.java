package com.desafio.infrastructure.api;

import com.desafio.infrastructure.api.pagination.Pagination;
import com.desafio.infrastructure.api.request.*;
import com.desafio.infrastructure.api.response.CompanyOutput;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.wildfly.common.annotation.Nullable;

import javax.validation.Valid;

@RequestMapping(value = "companies")
@Tag(name = "Companies")
public interface CompanyApi {

  @PostMapping(
          consumes = MediaType.APPLICATION_JSON_VALUE,
          produces = MediaType.APPLICATION_JSON_VALUE
  )
  @Operation(summary = "Create a new company")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "201", description = "Created successfully"),
          @ApiResponse(responseCode = "422", description = "A validation error was thrown"),
          @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
  })
  ResponseEntity<?> create(@RequestBody CreateCompanyRequest input);

  @GetMapping
  @Operation(summary = "Paginate all companies if you are not giving 'active' or by status")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Listed successfully"),
          @ApiResponse(responseCode = "422", description = "A invalid parameter was received"),
          @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
  })
  Pagination<CompanyOutput> listCompanies(
          @RequestParam(name = "page", required = false, defaultValue = "0") final int page,
          @RequestParam(name = "perPage", required = false, defaultValue = "10") final int perPage,
          @RequestParam(name = "active", required = false) @Nullable final Boolean sort,
          @RequestParam(name = "userCpf") final String userCpf
  );

  @PutMapping(
          value = "{ticker}/update",
          consumes = MediaType.APPLICATION_JSON_VALUE,
          produces = MediaType.APPLICATION_JSON_VALUE
  )
  @Operation(summary = "Update a company by it's identifier")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Company updated successfully"),
          @ApiResponse(responseCode = "404", description = "Company was not found"),
          @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
  })
  ResponseEntity<?> updateByTicker(@PathVariable(name = "ticker") String ticker, @RequestBody UpdateCompanyRequest input);

  @DeleteMapping(
          value = "{ticker}",
          produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @Operation(summary = "Delete a company by it's identifier")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "204", description = "Company deleted successfully"),
          @ApiResponse(responseCode = "404", description = "Company was not found"),
          @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
  })
  ResponseEntity<?> deleteByTicker(@PathVariable(name = "ticker") String ticker, @RequestBody DeleteCompanyRequest input);

  @PutMapping(
          value = "{ticker}/activate",
          consumes = MediaType.APPLICATION_JSON_VALUE,
          produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @Operation(summary = "Activate a company by it's identifier")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Company activated successfully"),
          @ApiResponse(responseCode = "404", description = "Company was not found"),
          @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
  })
  ResponseEntity<?> activate(@PathVariable(name = "ticker") String ticker, @RequestBody ActivationCompanyRequest input);

  @PutMapping(
          value = "{ticker}/deactivate",
          consumes = MediaType.APPLICATION_JSON_VALUE,
          produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @Operation(summary = "Deactivate a company by it's identifier")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Company activated successfully"),
          @ApiResponse(responseCode = "404", description = "Company was not found"),
          @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
  })
  ResponseEntity<?> deactivate(@PathVariable(name = "ticker") String ticker, @RequestBody ActivationCompanyRequest input);

  @GetMapping(
          value = "{ticker}",
          produces = MediaType.APPLICATION_JSON_VALUE
  )
  @Operation(summary = "Get a company by it's ticker")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Company retrieved successfully"),
          @ApiResponse(responseCode = "404", description = "Company was not found"),
          @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
  })
  ResponseEntity<?> findByTicker(@PathVariable(name = "ticker") String ticker, @RequestBody FindCompanyByTickerRequest input);
}

package com.desafio.infrastructure.api.response;

import com.desafio.application.company.ICompanyEntity;
import com.desafio.shared.notification.INotification;
import com.desafio.shared.notification.Notification;
import com.desafio.shared.notification.NotificationErrorProps;

import java.math.BigDecimal;
import java.time.Instant;

import static com.desafio.shared.utils.Constants.COMPANY_STR;

public record CompanyOutput(String id, String name, String ticker, BigDecimal price, Boolean isActive, Instant createdAt, Instant updatedAt, Instant deletedAt, INotification notification) {

  public static CompanyOutput of(ICompanyEntity company) {
    String id = company.id();
    String name = company.name();
    String ticker = company.ticker();
    BigDecimal price = company.price();
    Boolean isActive = company.isActive();
    Instant createdAt = company.createdAt();
    Instant updatedAt = company.updatedAt();
    Instant deletedAt = company.deletedAt();
    return new CompanyOutput(id, name, ticker, price, isActive, createdAt, updatedAt, deletedAt, null);
  }

  public static CompanyOutput of(INotification notification) {
    return new CompanyOutput(null, null, null, null,null, null, null, null, notification);
  }

  public static CompanyOutput of() {
    return new CompanyOutput(null, null, null, null,null, null, null, null, null);
  }

  public static CompanyOutput of(String message) {
    INotification notification = new Notification();
    notification.append(new NotificationErrorProps(message, COMPANY_STR));
    return new CompanyOutput(null, null, null, null, null, null, null, null, notification);
  }
}

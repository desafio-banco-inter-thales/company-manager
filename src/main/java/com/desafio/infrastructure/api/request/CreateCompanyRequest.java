package com.desafio.infrastructure.api.request;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public record CreateCompanyRequest(
        @NotNull String name,
        @NotNull String ticker,
        @NotNull
        @DecimalMin(value = "0.0", inclusive = false, message = "mensagem decimal")
        @Digits(integer=3, fraction=2, message = "mensagem digits")
        BigDecimal price,
        @NotNull String userCpf,
        @NotNull Boolean isActive) {

}

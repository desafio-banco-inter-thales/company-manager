package com.desafio.infrastructure.api.request;

public record ActivationCompanyRequest(String userCpf) {
}

package com.desafio.infrastructure.api.request;

public record DeleteCompanyRequest(String userCpf) { }

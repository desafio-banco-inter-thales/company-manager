package com.desafio.infrastructure.api.request;

public record FindCompanyByTickerRequest(String userCpf) {
}

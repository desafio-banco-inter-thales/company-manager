package com.desafio.infrastructure.api.request;

import java.math.BigDecimal;

public record UpdateCompanyRequest(BigDecimal price, String userCpf) {
}

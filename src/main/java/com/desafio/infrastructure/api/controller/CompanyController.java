package com.desafio.infrastructure.api.controller;

import com.desafio.infrastructure.api.CompanyApi;
import com.desafio.infrastructure.api.pagination.Pagination;
import com.desafio.infrastructure.api.pagination.SearchQuery;
import com.desafio.infrastructure.api.request.*;
import com.desafio.infrastructure.api.response.CompanyOutput;
import com.desafio.infrastructure.api.service.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

@RestController
public class CompanyController implements CompanyApi {

  private final CreateCompanyService createCompanyService;
  private final UpdateCompanyService updateCompanyService;
  private final ActivateCompanyService activateCompanyService;
  private final DeactivateCompanyService deactivateCompanyService;
  private final DeleteCompanyService deleteCompanyService;
  private final ListCompaniesService listCompaniesService;
  private final FindCompanyByTickerService findCompanyByTickerService;

  private CompanyController(
          CreateCompanyService createCompanyService,
          UpdateCompanyService updateCompanyService,
          ActivateCompanyService activateCompanyService,
          DeactivateCompanyService deactivateCompanyService,
          DeleteCompanyService deleteCompanyService,
          ListCompaniesService listCompaniesService,
          FindCompanyByTickerService findCompanyByTickerService) {
    this.createCompanyService = createCompanyService;
    this.updateCompanyService = updateCompanyService;
    this.activateCompanyService = activateCompanyService;
    this.deactivateCompanyService = deactivateCompanyService;
    this.deleteCompanyService = deleteCompanyService;
    this.listCompaniesService = listCompaniesService;
    this.findCompanyByTickerService = findCompanyByTickerService;
  }


  @Override
  public ResponseEntity<?> create(CreateCompanyRequest input) {
    final var response = this.createCompanyService.execute(input);

    if (response.notification() != null) {
      return ResponseEntity.unprocessableEntity().body(response);
    } else {
      String userLocation = "/companies/".concat(input.ticker());
      return ResponseEntity.created(URI.create(userLocation)).build();
    }
  }

  @Override
  public Pagination<CompanyOutput> listCompanies(int page, int perPage, Boolean isActive, String userCpf) {
    return listCompaniesService.execute(new SearchQuery(page, perPage, isActive, userCpf));
  }

  @Override
  public ResponseEntity<?> updateByTicker(String ticker, UpdateCompanyRequest input) {
    final var response = this.updateCompanyService.execute(ticker, input);

    if (response.notification() != null) {
      return ResponseEntity.unprocessableEntity().body(response);
    } else {
      return ResponseEntity.ok(response);
    }
  }

  @Override
  public ResponseEntity<?> activate(String ticker, ActivationCompanyRequest input) {
    final var response = this.activateCompanyService.execute(ticker, input);

    if (response.notification() != null) {
      return ResponseEntity.unprocessableEntity().body(response);
    } else {
      return ResponseEntity.ok(response);
    }
  }

  @Override
  public ResponseEntity<?> deactivate(String ticker,  ActivationCompanyRequest input) {
    final var response = this.deactivateCompanyService.execute(ticker, input);

    if (response.notification() != null) {
      return ResponseEntity.unprocessableEntity().body(response);
    } else {
      return ResponseEntity.ok(response);
    }
  }

  @Override
  public ResponseEntity<?> deleteByTicker(String ticker, DeleteCompanyRequest input) {
    String userCpf = input.userCpf();
    final var response= this.deleteCompanyService.execute(ticker, userCpf);
    return response.notification() != null ?
            ResponseEntity.unprocessableEntity().body(response.notification())
            :
            ResponseEntity.status(HttpStatus.valueOf(204)).build();
  }

  @Override
  public ResponseEntity<?> findByTicker(String ticker, FindCompanyByTickerRequest input) {
    CompanyOutput response = this.findCompanyByTickerService.execute(ticker, input.userCpf());
    return response.notification() != null ?
            ResponseEntity.notFound().build()
            :
            ResponseEntity.ok().body(response);
  }
}
